package main

import (
	"gitlab.com/king011/go-socks5/cmd"
	"log"
)

func main() {
	if e := cmd.Execute(); e != nil {
		log.Fatalln(e)
	}
}
