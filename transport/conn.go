package transport

import (
	"context"
	"net"
	"sync"
	"time"
)

// Conn 包裝 net.Conn 一定時間 未活躍 關閉
type Conn struct {
	net.Conn
	sync.Mutex

	timeout time.Duration
	last    time.Time
	closed  bool

	ctx    context.Context
	cancel context.CancelFunc
}

// NewConn .
func NewConn(c net.Conn, timeout time.Duration) *Conn {
	ctx, cancel := context.WithCancel(context.Background())
	cc := &Conn{
		c,
		sync.Mutex{},

		timeout,
		time.Now(),
		false,

		ctx,
		cancel,
	}
	go cc.check()
	return cc
}
func (c *Conn) check() {
	done := c.ctx.Done()
	timer := time.NewTimer(c.timeout)
	runTimer := true
	for {
		select {
		case <-done:
			// 結束 退出
			if runTimer && !timer.Stop() {
				<-timer.C
			}
			return
		case now := <-timer.C:
			c.Lock()
			last := c.last
			c.Unlock()
			if now.Before(last.Add(c.timeout)) {
				// 啓動 新 定時器
				timer.Reset(c.timeout)
			} else {
				runTimer = false
				// 超時 關閉
				c.Close()
			}
		}
	}
}
func (c *Conn) Write(b []byte) (n int, e error) {
	n, e = c.Conn.Write(b)
	if e == nil && n != 0 {
		c.Lock()
		c.last = time.Now()
		c.Unlock()
	}
	return
}
func (c *Conn) Read(b []byte) (n int, e error) {
	n, e = c.Conn.Read(b)
	if e == nil && n != 0 {
		c.Lock()
		c.last = time.Now()
		c.Unlock()
	}
	return
}

// Close .
func (c *Conn) Close() (e error) {
	e = c.Conn.Close()
	if e != nil {
		return
	}
	c.cancel()
	return
}
