package transport

import (
	"bytes"
	"errors"
	"fmt"
	"net"
	"time"

	"gitlab.com/king011/king-go/io/call"
	"gitlab.com/king011/king-go/io/message"
	"golang.org/x/net/context"
	"google.golang.org/grpc/credentials"
)

const (
	_CommandAuth    = 100
	_CommandSuccess = 200
	_CommandError   = 500
)

var errPasswordNotMatch = errors.New("password not match")

type _AuthInfo struct {
}

func (_AuthInfo) AuthType() string {
	return "transport auth by password"
}

type _TransportCredentials struct {
	isServer bool
	pwd      []byte
	base     credentials.TransportCredentials
}

// NewServerCreds 創建 密碼驗證 服務器
func NewServerCreds(base credentials.TransportCredentials, pwd []byte) credentials.TransportCredentials {
	return &_TransportCredentials{
		isServer: true,
		pwd:      pwd,
		base:     base,
	}
}

// NewClientCreds 創建 h2c 密碼驗證 客戶端
func NewClientCreds(base credentials.TransportCredentials, pwd []byte) credentials.TransportCredentials {
	return &_TransportCredentials{
		isServer: false,
		pwd:      pwd,
		base:     base,
	}
}

// 實現 客戶端 向 服務器 請求認證
func (t *_TransportCredentials) ClientHandshake(ctx context.Context, str string, c net.Conn) (net.Conn, credentials.AuthInfo, error) {
	if t.base != nil {
		// 進行原始 驗證
		c0, i, e := t.base.ClientHandshake(ctx, str, c)
		if e != nil {
			return c0, i, e
		}
		c = c0
	}

	// 創建 session
	session := call.NewSession(
		message.NewParser(message.NewDefaultAnalyst()),
		c,
	)
	// 向 服務器 請求 身份認證
	request, e := message.Marshal(_CommandAuth, []byte(t.pwd))
	if e != nil {
		return nil, nil, e
	}
	response, e := session.Call(ctx, request)
	if e != nil {
		return nil, nil, e
	}
	cmd := message.Command(response)
	if cmd == _CommandError {
		return nil, nil, errors.New(string(message.Body(response)))
	} else if cmd != _CommandSuccess {
		return nil, nil, fmt.Errorf("unknow commnad [%v]", cmd)
	}

	// 返回 net.Conn
	return c, _AuthInfo{}, nil
}

// 實現 服務器 驗證 客戶端
func (t *_TransportCredentials) ServerHandshake(c net.Conn) (net.Conn, credentials.AuthInfo, error) {
	if t.base != nil {
		// 進行原始 驗證
		c0, i, e := t.base.ServerHandshake(c)
		if e != nil {
			return c0, i, e
		}
		c = c0
	}

	// 創建 session
	session := call.NewSession(
		message.NewParser(message.NewDefaultAnalyst()),
		c,
	)
	// 獲取 客戶端 發來的 驗證
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	request, e := session.Recv(ctx)
	if e != nil {
		return nil, nil, e
	}

	// 驗證 密碼
	cmd := message.Command(request)
	if cmd != _CommandAuth {
		return nil, nil, fmt.Errorf("not support command [%v]", cmd)
	}
	body := message.Body(request)

	if !bytes.Equal(body, t.pwd) {
		// 通知 客戶端 失敗
		response, e := message.Marshal(_CommandError, []byte(errPasswordNotMatch.Error()))
		if e != nil {
			return nil, nil, errPasswordNotMatch
		}
		session.Send(ctx, response)

		return nil, nil, errPasswordNotMatch
	}

	// 通知 客戶端 成功
	response, e := message.Marshal(_CommandSuccess, nil)
	if e != nil {
		return nil, nil, e
	}
	session.Send(ctx, response)

	//返回 net.Conn
	return c, _AuthInfo{}, nil
}

func (t *_TransportCredentials) Info() credentials.ProtocolInfo {
	return credentials.ProtocolInfo{}
}
func (t *_TransportCredentials) Clone() credentials.TransportCredentials {
	tn := *t
	return &tn
}

func (t *_TransportCredentials) OverrideServerName(string) error {
	return nil
}
