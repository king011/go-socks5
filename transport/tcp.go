package transport

import (
	"crypto/tls"
	"net"
	"time"
)

// DialF .
type DialF func(string, time.Duration) (net.Conn, error)

// TCPDial .
func TCPDial(useTLS, skipVerify bool) (name string, f DialF) {
	if useTLS {
		if skipVerify {
			name = "tcp-h2-skip"
			f = tcpDialTLSSkip
		} else {
			name = "tcp-h2"
			f = tcpDialTLS
		}
	} else {
		name = "tcp-h2c"
		f = tcpDial
	}
	return
}
func tcpDialTLSSkip(addr string, timeout time.Duration) (c net.Conn, e error) {
	dialer := new(net.Dialer)
	dialer.Timeout = timeout
	c, e = tls.DialWithDialer(dialer, "tcp", addr, &tls.Config{
		InsecureSkipVerify: true,
	})
	return
}
func tcpDialTLS(addr string, timeout time.Duration) (c net.Conn, e error) {
	dialer := new(net.Dialer)
	dialer.Timeout = timeout
	c, e = tls.DialWithDialer(dialer, "tcp", addr, &tls.Config{
		InsecureSkipVerify: false,
	})
	return
}

func tcpDial(addr string, timeout time.Duration) (net.Conn, error) {
	return net.DialTimeout("tcp", addr, timeout)
}

// TCPListen 創建 tcp listener
func TCPListen(addr, certFile, keyFile string, timeout time.Duration) (name string, l net.Listener, e error) {
	if certFile != "" && keyFile != "" {
		var cert tls.Certificate
		cert, e = tls.LoadX509KeyPair(certFile, keyFile)
		if e != nil {
			return
		}
		l, e = tls.Listen("tcp", addr, &tls.Config{
			//配置 證書
			Certificates: []tls.Certificate{cert},
		})
		if e != nil {
			return
		}
		name = "tcp-h2"
	} else {
		l, e = net.Listen("tcp", addr)
		if e != nil {
			return
		}
		name = "tcp-h2c"
	}
	return
}
