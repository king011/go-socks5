package transport

import (
	"crypto/tls"
	"fmt"
	"net"
	"time"

	kcp "github.com/xtaci/kcp-go"
)

// KCPDialer .
type KCPDialer struct {
	Name                     string
	Dial                     DialF
	dataShards, parityShards int
}

func (k *KCPDialer) dial(addr string, timeout time.Duration) (c net.Conn, e error) {
	return kcp.DialWithOptions(addr, nil, k.dataShards, k.parityShards)
}
func (k *KCPDialer) dialTLS(addr string, timeout time.Duration) (c net.Conn, e error) {
	return DialTLS(addr, timeout,
		&tls.Config{
			InsecureSkipVerify: false,
		},
		k.dial,
	)
}
func (k *KCPDialer) dialTLSSkip(addr string, timeout time.Duration) (c net.Conn, e error) {
	return DialTLS(addr, timeout,
		&tls.Config{
			InsecureSkipVerify: true,
		},
		k.dial,
	)
}

// KCPDial .
func KCPDial(useTLS, skipVerify bool, dataShards, parityShards int) (dialer *KCPDialer) {
	if useTLS {
		if skipVerify {
			dialer = &KCPDialer{
				Name:         fmt.Sprintf("kcp-h2-skip %v %v", dataShards, parityShards),
				dataShards:   dataShards,
				parityShards: parityShards,
			}
			dialer.Dial = dialer.dialTLSSkip
		} else {
			dialer = &KCPDialer{
				Name:         fmt.Sprintf("kcp-h2 %v %v", dataShards, parityShards),
				dataShards:   dataShards,
				parityShards: parityShards,
			}
			dialer.Dial = dialer.dialTLS
		}
	} else {
		dialer = &KCPDialer{
			Name:         fmt.Sprintf("kcp-h2c %v %v", dataShards, parityShards),
			dataShards:   dataShards,
			parityShards: parityShards,
		}
		dialer.Dial = dialer.dial
	}
	return
}

// KCPListen 創建 kcp listener
func KCPListen(addr, certFile, keyFile string,
	timeout time.Duration,
	dataShards, parityShards int,
) (name string, l net.Listener, e error) {
	if certFile != "" && keyFile != "" {
		var cert tls.Certificate
		cert, e = tls.LoadX509KeyPair(certFile, keyFile)
		if e != nil {
			return
		}
		l, e = kcp.ListenWithOptions(addr, nil, dataShards, parityShards)
		if e != nil {
			return
		}
		l = tls.NewListener(l, &tls.Config{
			//配置 證書
			Certificates: []tls.Certificate{cert},
		})
		name = fmt.Sprintf("kcp-h2 %v %v", dataShards, parityShards)
	} else {
		l, e = kcp.ListenWithOptions(addr, nil, dataShards, parityShards)
		if e != nil {
			return
		}
		name = fmt.Sprintf("kcp-h2c %v %v", dataShards, parityShards)
	}

	return
}
