package transport

import (
	"crypto/tls"
	"net"
	"strings"
	"time"
)

type timeoutError struct{}

func (timeoutError) Error() string   { return "tls: DialWithDialer timed out" }
func (timeoutError) Timeout() bool   { return true }
func (timeoutError) Temporary() bool { return true }

var emptyConfig tls.Config

func defaultConfig() *tls.Config {
	return &emptyConfig
}

// DialTLS 撥號 並 使用 tls
func DialTLS(addr string, timeout time.Duration, cnf *tls.Config, f DialF) (net.Conn, error) {
	var errChannel chan error
	var timer *time.Timer
	if timeout > 0 {
		errChannel = make(chan error, 2)
		timer = time.AfterFunc(timeout, func() {
			errChannel <- timeoutError{}
		})
	}

	c, e := f(addr, timeout)
	if e != nil {
		if timer != nil {
			timer.Stop()
		}
		return nil, e
	}

	c, e = clientTLS(addr, c, errChannel, cnf)
	if e != nil {
		return nil, e
	}

	if timer != nil {
		timer.Stop()
	}
	return c, nil
}
func clientTLS(addr string, rawConn net.Conn,
	errChannel chan error,
	config *tls.Config,
) (net.Conn, error) {
	// rawConn, err := dialer.Dial(network, addr)
	// if err != nil {
	// 	return nil, err
	// }
	colonPos := strings.LastIndex(addr, ":")
	if colonPos == -1 {
		colonPos = len(addr)
	}
	hostname := addr[:colonPos]

	if config == nil {
		config = defaultConfig()
	}

	// If no ServerName is set, infer the ServerName
	// from the hostname we're connecting to.
	if config.ServerName == "" {
		// Make a copy to avoid polluting argument or default.
		c := config.Clone()
		c.ServerName = hostname
		config = c
	}

	conn := tls.Client(rawConn, config)
	var err error
	if errChannel == nil {
		err = conn.Handshake()
	} else {
		go clientTLSHandshake(conn, errChannel)
		err = <-errChannel
	}

	if err != nil {
		rawConn.Close()
		return nil, err
	}

	return conn, nil
}
func clientTLSHandshake(conn *tls.Conn, errChannel chan error) {
	errChannel <- conn.Handshake()
}
