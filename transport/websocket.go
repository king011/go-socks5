package transport

import (
	"context"
	"crypto/tls"
	"errors"
	"net"
	"net/http"
	"sync"
	"time"

	"gitlab.com/king011/go-socks5/transport/websocket"
)

const (
	// WebsocketURL websocket url
	WebsocketURL = "/websocket/go/x"
)

// WebsocketDial .
func WebsocketDial(useTLS, skipVerify bool) (name string, f DialF) {
	if useTLS {
		if skipVerify {
			name = "wss-skip"
			f = wssDialTLSSkip
		} else {
			name = "wss"
			f = wssDialTLS
		}
	} else {
		name = "ws"
		f = wsDial
	}
	return
}
func wssDialTLSSkip(addr string, timeout time.Duration) (net.Conn, error) {
	url := "wss://" + addr + WebsocketURL
	origin := "https://" + addr + WebsocketURL

	config, err := websocket.NewConfig(url, origin)
	if err != nil {
		return nil, err
	}
	config.Dialer = &net.Dialer{
		Timeout: timeout,
	}
	config.TlsConfig = &tls.Config{
		InsecureSkipVerify: true,
	}
	ws, e := websocket.DialConfig(config)
	if e != nil {
		return nil, e
	}
	return websocket.NewRawConn(ws), nil
}
func wssDialTLS(addr string, timeout time.Duration) (net.Conn, error) {
	url := "wss://" + addr + WebsocketURL
	origin := "https://" + addr + WebsocketURL

	config, err := websocket.NewConfig(url, origin)
	if err != nil {
		return nil, err
	}
	config.Dialer = &net.Dialer{
		Timeout: timeout,
	}
	config.TlsConfig = &tls.Config{
		InsecureSkipVerify: false,
	}
	ws, e := websocket.DialConfig(config)
	if e != nil {
		return nil, e
	}
	return websocket.NewRawConn(ws), nil
}
func wsDial(addr string, timeout time.Duration) (net.Conn, error) {
	url := "ws://" + addr + WebsocketURL
	origin := "http://" + addr + WebsocketURL

	config, err := websocket.NewConfig(url, origin)
	if err != nil {
		return nil, err
	}
	config.Dialer = &net.Dialer{
		Timeout: timeout,
	}
	ws, e := websocket.DialConfig(config)
	if e != nil {
		return nil, e
	}
	return websocket.NewRawConn(ws), nil
}

type _WebsocketListen struct {
	l      net.Listener
	ctx    context.Context
	cancel context.CancelFunc
	ch     chan *_WebsocketConn
	closed bool
	mutex  sync.Mutex
}

// WebsocketListen 創建 Websocket listener
func WebsocketListen(addr, certFile, keyFile string, timeout time.Duration) (name string, l net.Listener, e error) {
	if certFile != "" && keyFile != "" {
		var cert tls.Certificate
		cert, e = tls.LoadX509KeyPair(certFile, keyFile)
		if e != nil {
			return
		}
		l, e = tls.Listen("tcp", addr, &tls.Config{
			//配置 證書
			Certificates: []tls.Certificate{cert},
		})
		if e != nil {
			return
		}
		name = "wss"
	} else {
		l, e = net.Listen("tcp", addr)
		if e != nil {
			return
		}
		name = "ws"
	}
	ctx, cancel := context.WithCancel(context.Background())
	wl := &_WebsocketListen{
		l:      l,
		ctx:    ctx,
		cancel: cancel,
		ch:     make(chan *_WebsocketConn),
	}
	go wl.serve()
	l = wl
	return
}

type _WebsocketConn struct {
	net.Conn
	ch chan struct{}
	m  sync.Mutex
}

func (c *_WebsocketConn) Close() (e error) {
	c.m.Lock()
	ch := c.ch
	if ch == nil {
		e = errors.New("websocket conn closed")
	} else {
		c.ch = nil
	}
	c.m.Unlock()

	if ch != nil {
		c.Conn.Close()
		<-ch
	}
	return
}
func (l *_WebsocketListen) serve() {
	http.Handle(WebsocketURL, websocket.Handler(l.onConnected))
	http.Serve(l.l, nil)
}
func (l *_WebsocketListen) onConnected(ws *websocket.Conn) {
	ch := make(chan struct{})
	rc := websocket.NewRawConn(ws)
	c := &_WebsocketConn{rc, ch, sync.Mutex{}}
	select {
	case <-l.ctx.Done():
		rc.Close()
		return
	case l.ch <- c:
	}
	<-ch
}

// Accept waits for and returns the next connection to the listener.
func (l *_WebsocketListen) Accept() (net.Conn, error) {
	select {
	case <-l.ctx.Done():
		return nil, l.ctx.Err()
	case c := <-l.ch:
		return c, nil
	}
}

// Close closes the listener.
// Any blocked Accept operations will be unblocked and return errors.
func (l *_WebsocketListen) Close() (e error) {
	l.mutex.Lock()
	if l.closed {
		e = errors.New("websocket listener closed")
	} else {
		l.closed = true
	}
	l.mutex.Unlock()
	if e != nil {
		return
	}
	e = l.l.Close()
	l.cancel()
	return
}

// Addr returns the listener's network address.
func (l *_WebsocketListen) Addr() net.Addr {
	return l.l.Addr()
}
