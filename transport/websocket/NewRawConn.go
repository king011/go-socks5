package websocket

import (
	"net"
)

type _RawConn struct {
	*Conn
}

// NewRawConn .
func NewRawConn(ws *Conn) (c net.Conn) {
	return _RawConn{ws}
}
func (c _RawConn) Write(b []byte) (n int, e error) {
	n, e = c.rwc.Write(b)
	return
}
func (c _RawConn) Read(b []byte) (n int, e error) {
	n, e = c.rwc.Read(b)
	return
}
func (c _RawConn) Close() (e error) {
	return c.rwc.Close()
}
