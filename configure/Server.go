package configure

import (
	"encoding/json"
	"io/ioutil"

	jsonnet "github.com/google/go-jsonnet"
	"gitlab.com/king011/go-socks5/configure/server"
	logger "gitlab.com/king011/king-go/log/logger.zap"
)

// Server 服務器 配置
type Server struct {
	Server []server.Server
	Logger logger.Options
}

// Format 格式化 參數
func (s *Server) Format(basePath string) (e error) {
	for i := 0; i < len(s.Server); i++ {
		e = s.Server[i].Format(basePath)
		if e != nil {
			return
		}
	}
	return
}
func (s *Server) String() string {
	if s == nil {
		return "nil"
	}
	b, e := json.MarshalIndent(s, "", "\t")
	if e != nil {
		return e.Error()
	}
	return string(b)
}

// Load 加載 配置
func (s *Server) Load(filename string) (e error) {
	var b []byte
	b, e = ioutil.ReadFile(filename)
	if e != nil {
		return
	}
	vm := jsonnet.MakeVM()
	var jsonStr string
	jsonStr, e = vm.EvaluateSnippet("", string(b))
	if e != nil {
		return
	}
	b = []byte(jsonStr)
	e = json.Unmarshal(b, s)
	if e != nil {
		return
	}
	return
}
