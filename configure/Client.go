package configure

import (
	"encoding/json"
	"io/ioutil"

	jsonnet "github.com/google/go-jsonnet"
	"gitlab.com/king011/go-socks5/configure/client"
	"gitlab.com/king011/go-socks5/configure/general"
	logger "gitlab.com/king011/king-go/log/logger.zap"
)

// Client socks5 配置
type Client struct {
	Proxy     client.Proxy
	DNS       client.DNS
	GRPC      client.GRPC
	Transport client.Transport
	NET       general.NET
	Logger    logger.Options
}

// Format 格式化 參數
func (c *Client) Format() (e error) {
	e = c.Proxy.Format()
	if e != nil {
		return
	}
	e = c.DNS.Format()
	if e != nil {
		return
	}
	e = c.GRPC.Format()
	if e != nil {
		return
	}
	e = c.Transport.Format()
	if e != nil {
		return
	}
	e = c.NET.Format()
	if e != nil {
		return
	}

	return
}
func (c *Client) String() string {
	if c == nil {
		return "nil"
	}
	b, e := json.MarshalIndent(c, "", "\t")
	if e != nil {
		return e.Error()
	}
	return string(b)
}

// Load 加載 配置
func (c *Client) Load(filename string) (e error) {
	var b []byte
	b, e = ioutil.ReadFile(filename)
	if e != nil {
		return
	}
	vm := jsonnet.MakeVM()
	var jsonStr string
	jsonStr, e = vm.EvaluateSnippet("", string(b))
	if e != nil {
		return
	}
	b = []byte(jsonStr)
	e = json.Unmarshal(b, c)
	if e != nil {
		return
	}
	return
}
