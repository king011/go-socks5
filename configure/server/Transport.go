package server

import (
	"strings"

	"gitlab.com/king011/go-socks5/utils"
)

// Transport 數據傳輸方式 定義
type Transport struct {
	// tcp kcp websocket
	Protocol string

	// x509 證書路徑
	CertFile string
	KeyFile  string

	// kcp 參數
	DataShards   int
	ParityShards int
}

// Format 格式化 配置
func (t *Transport) Format(bashPath string) (e error) {
	t.Protocol = strings.TrimSpace(t.Protocol)
	if t.Protocol == "" {
		t.Protocol = "tcp"
	} else {
		t.Protocol = strings.ToLower(t.Protocol)
	}

	t.CertFile = strings.TrimSpace(t.CertFile)
	t.KeyFile = strings.TrimSpace(t.KeyFile)
	if t.CertFile != "" {
		t.CertFile = utils.Abs(bashPath, t.CertFile)
	}
	if t.KeyFile != "" {
		t.KeyFile = utils.Abs(bashPath, t.KeyFile)
	}
	return
}
