package server

import (
	"gitlab.com/king011/go-socks5/configure/general"
)

// Server 服務器
type Server struct {
	GRPC GRPC
	// 如果爲 true 開啓 dns 轉發
	DNS       bool
	NET       general.NET
	Transport Transport
}

// Format 格式化 配置
func (s *Server) Format(bashPath string) (e error) {
	e = s.GRPC.Format()
	if e != nil {
		return
	}
	e = s.NET.Format()
	if e != nil {
		return
	}
	e = s.Transport.Format(bashPath)
	if e != nil {
		return
	}
	return
}
