package server

import (
	"strings"
	"time"
)

// GRPC 定義
type GRPC struct {
	// 服務器監聽地址
	Addr string

	// 客戶端 驗證 密碼
	Password string

	// 定義向 客戶端 發送 ping 以 檢測 網路狀態
	Ping time.Duration

	// 未活躍連接 被斷開 時間 (單位:秒)
	Timeout time.Duration
}

// Format 格式化 配置
func (g *GRPC) Format() (e error) {
	g.Addr = strings.TrimSpace(g.Addr)

	if g.Timeout > 0 {
		g.Timeout *= time.Second
	} else {
		g.Timeout = time.Minute
	}
	return
}
