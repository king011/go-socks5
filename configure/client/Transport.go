package client

import (
	"strings"
)

// Transport 數據傳輸方式 定義
type Transport struct {
	// tcp kcp websocket
	Protocol string

	// 是否使用 TLS 安全 傳輸
	TLS bool
	// 是否 忽略 證書驗證
	SkipVerify bool

	// kcp 參數
	DataShards   int
	ParityShards int
}

// Format 格式化 配置
func (t *Transport) Format() (e error) {
	t.Protocol = strings.TrimSpace(t.Protocol)
	if t.Protocol == "" {
		t.Protocol = "tcp"
	} else {
		t.Protocol = strings.ToLower(t.Protocol)
	}

	return
}
