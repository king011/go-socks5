package client

import (
	"strings"
)

// Proxy 代理設置
type Proxy struct {
	// socks5 代理 監聽地址
	// 如果 爲空 則不創建 socks5 代理
	Socks5 string

	// 透明代理 監聽地址
	// 如果爲空 不 運行透明代理
	// windows 不支持此功能
	Redir string
}

// Format 格式化 配置
func (p *Proxy) Format() (e error) {
	p.Socks5 = strings.TrimSpace(p.Socks5)
	p.Redir = strings.TrimSpace(p.Redir)
	return
}
