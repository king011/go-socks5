package client

import (
	"strings"
)

// DNS dns 服務器 配置
type DNS struct {
	// dns 服務器 監聽地址
	Addr string
	// 向哪個 dns 服務器 請求 解析域名
	Server string
}

// Format 格式化 配置
func (d *DNS) Format() (e error) {
	d.Addr = strings.TrimSpace(d.Addr)
	d.Server = strings.TrimSpace(d.Server)
	return
}
