package client

import (
	"strings"
	"time"
)

// GRPC 服務器 配置
type GRPC struct {
	// 服務器 地址
	Addr string
	// 密碼
	Password string

	// 定義向 服務器 發送 ping 以免網路未活躍連接 被斷開 (單位:秒)
	Ping time.Duration

	// 未活躍連接 被斷開 時間 (單位:秒)
	Timeout time.Duration
}

// Format 格式化 配置
func (g *GRPC) Format() (e error) {
	g.Addr = strings.TrimSpace(g.Addr)
	if g.Ping > 0 {
		g.Ping *= time.Second
	} else {
		g.Ping = time.Second * 10
	}

	if g.Timeout > 0 {
		g.Timeout *= time.Second
	} else {
		g.Timeout = time.Second * 20
	}
	return
}
