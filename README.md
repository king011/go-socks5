# go-socks5

golang grpc 實現的 socks5 代理和 dns 轉發 程式


提供了如下功能

* socks5 代理 解決 網路封鎖
* 轉發 dns 請求 解決 dns 污染
* redir 爲 linux 提供透明代理
* 客戶端/服務器 支持多種傳輸模式 tcp/kcp/websocket


go-socks5 基本上和 shadowsocks/shadowsocksrr 是相同功能的東西 用於科學上網 可惡的資本主義社會永遠不會知道 上網是多麼奢侈的一件事

go-socks5 和 shadowsocks/shadowsocksrr 相比 有如下 特點


1. 使用go語言實現 代碼量/複雜度 都比 c 的 shadowsocks/shadowsocksrr 少很多 所以 編譯 和 改寫會 簡單很多
2. 使用 grpc 通信 可以使用 tls 確保 安全 且 服務器和客戶端 只存在 一條 http2 的 連接
3. 因爲 grpc 是 http2 協議 相比私有協議可以 配置一些通用的 http工具 比如 nginx
4. 所有功能 都由 go-socks5 實現 部署 服務器 和 客戶端 會比較簡單


# 安裝

請到 [tags](/../tags) 頁面 下載 最新 tar包 解壓 即可

# 傳輸層

go-socks5 目前 支持 三種 傳輸層設置

1. tcp
2. websocket 
3. kcp

## tcp
這是 grpc 默認的 傳輸 方式 外部看起來是 http2 

雖然 nginx 已經支持 grpc 但不要使用 nginx 作爲反向代理 因爲 nginx 目前 將爲每個請求 創建一個 到 grpc的 新連接 

如果 現在 有10個 代理請求 
* client -> nginx : grpc 協議只有 1個 http2 連接
* nginx -> grpc : 有10個請求 所以有 10個 連接
* grpc -> 代理目標 : 有10個連接

所以 nginx 作爲 grpc 反向代理時 nginx 和grpc間 是沒有利用 http2提供的 tcp 復用的

# websocket

推薦的使用方式

從 v2.1.0 開始 websocket 方式 不再是 完整的 websocket 協議

websocket 方式 只是 使用 websocket 協議 建立了 連接 之後 數據傳輸 會回到 tcp 所以 不會有 數據傳輸時 封websocket幀的 消耗

因爲 使用 websocket 建立連接 所以 nginx 可以識別出 協議 故可以用來作爲 反向代理 


如果 現在 有10個 代理請求 
* client -> nginx : grpc 協議只有 1個 websocket 連接
* nginx -> grpc : 雖然有10個請求 但nginx只看到一個 websocket 請求 所以 到 grpc 也只有 一個 websocket 連接
* grpc -> 代理目標 : 有10個連接

這種方式 可以 僞裝成 websocket 並且 很好的使用 nginx 作爲 反向代理 是 我比較推薦的 一種方式

並且 因爲 grpc 復用了 tcp 所以 websocket 的連接 消耗 只會在 客戶端運行時 消耗一次 後面 都是 復用的 此tcp 和 使用 tcp grpc 間幾乎感覺不到額外資源 消耗

# kcp

只是 試驗性的 功能 不建議使用

# 編譯

1. 確保已安裝好 go 環境
2. 確保已經 安裝好 go grpc 環境
3. 執行 `go get -u -d gitlab.com/king011/go-socks5` 下載源碼
4. 切換到 gitlab.com/king011/go-socks5 目錄
5. 執行 `./build-pb.sh` 生成 grpc 代碼
6. 執行 `./build-go.sh linux` 或 `./build-go.sh windows` 編譯 linux/windows 下的 程式


# 子命令

go-socks5 只有兩個子命令 server 和 client

* server 運行 服務器 你應該在一臺 可以自由訪問網路的 地方 運行她
* client 和 server 建立通信 並 在本地 運行 socks5/dns/redir 服務器


# server

server 默認 讀取 go-socks5 所在位置下的 server.jsonnet 檔案 並安此檔案 配置 運行服務器 

傳入 -c 參數 可指定 配置檔案

```
$ ./go-socks5 server -h
run server

Usage:
  go-socks5 server [flags]

Flags:
  -c, --configure string   configure file path (default "server.jsonnet")
  -d, --debug              run as debug mode
  -h, --help               help for server
```

```
{
	// 服務器 配置 可配置多個 服務器
	Server:[
		{
			// GRPC 配置
			GRPC:{
				// 服務器監聽地址
				Addr:":10700",

				// 客戶端 驗證 密碼
				Password:"cerberus is an idea",
				
				// 未活躍連接 被斷開 時間 (單位:秒)
				Timeout:60,
			},
			// 如果爲 true 開啓 dns 轉發
			DNS:true,
			// 路網 配置
			NET:{
				// 請求超時 時間 (單位:秒)
				// 默認 10 秒
				Request:10,
				// 網橋 未活躍 超時 斷線時間  (單位:秒)
				// 默認 5 分鐘
				Bridge:60*5,
				// 每個連接 網路數據包 緩衝區大小
				// 默認 1024 * 32
				Buffer:1024*32
			},
			// 數據傳輸方式 定義
			Transport:{
				// tcp kcp websocket
				Protocol:"tcp",

				// x509 證書路徑
				CertFile:"server.pem",
				KeyFile:"server.key",
			},
		},
	],
	Logger:{
		// 日誌 http 如果爲空 則不啓動 http
		//HTTP:"localhost:20700",
		// 日誌 檔案名 如果爲空 則輸出到控制檯
		//Filename:"logs/server.log",
		// 單個日誌檔案 大小上限 MB
		//MaxSize:    100, 
		// 保存 多少個 日誌 檔案
		//MaxBackups: 3,
		// 保存 多少天內的 日誌
		//MaxAge:     28,
		// 要 保存的 日誌 等級 debug info warn error dpanic panic fatal
		Level :"info",
		// 是否要 輸出 代碼位置
		// Caller:true,
	},
}
```
> Server 是服務器 數組 可以定義 多個 服務器 以供 不過的 客戶端 連接
>
> 如果要 確保數據安全 請指定 Transport.CertFile Transport.KeyFile 設置 x509 證書檔案 位置
>
> 客戶端的 Password 必須和 服務器 GRPC.Password 一致
>

# client

client 默認 讀取 go-socks5 所在位置下的 client.jsonnet 檔案 並安此檔案 運行代理程式 

傳入 -c 參數 可指定 配置檔案

```
$ ./go-socks5 client -h
run client

Usage:
  go-socks5 client [flags]

Flags:
  -c, --configure string   configure file path (default "client.jsonnet")
  -d, --debug              run as debug mode
  -h, --help               help for client
      --password string    connect password
  -p, --ping int           ping server (default -1)
      --remote string      remote server address
  -t, --transport string   transport [tcp tcp-h2 tcp-h2-skip kcp kcp-h2 kcp-h2-skip ws wss wss-skip]
```

```
{
	// 代理 配置
	Proxy:{
		// socks5 代理 監聽地址
		// 如果 爲空 則不創建 socks5 代理
		Socks5:"localhost:1080",

		// 透明代理 監聽地址
		// 如果爲空 不 運行透明代理
		// windows 不支持此功能
		Redir:":10900",
	},
	// dns 服務器 配置
	DNS:{
		// dns 服務器 監聽地址
		Addr:":10054",
		// 向哪個 dns 服務器 請求 解析域名
		Server:"8.8.8.8:53",
	},
	// 服務器 配置
	GRPC:{
		// 服務器 地址
		Addr:"127.0.0.1:10700",
		// 密碼
		Password:"cerberus is an idea",

		// 定義向 服務器 發送 ping 以免網路未活躍連接 被斷開 (單位:秒)
		Ping:10,
	},
	// 數據傳輸方式 定義
	Transport:{
		// tcp kcp websocket
		Protocol:"tcp",

		// 是否使用 TLS 安全 傳輸
		TLS:true,
		// 是否 忽略 證書驗證
		SkipVerify:true,
	},
	// 路網 配置
	NET:{
		// 請求超時 時間 (單位:秒)
		// 默認 10 秒
		Request:10,
		// 網橋 未活躍 超時 斷線時間  (單位:秒)
		// 默認 5 分鐘
		Bridge:60*5,
		// 每個連接 網路數據包 緩衝區大小
		// 默認 1024 * 32
		Buffer:1024*32
	},
	Logger:{
		// 日誌 http 如果爲空 則不啓動 http
		//HTTP:"localhost:20800",
		// 日誌 檔案名 如果爲空 則輸出到控制檯
		//Filename:"logs/client.log",
		// 單個日誌檔案 大小上限 MB
		//MaxSize:    100, 
		// 保存 多少個 日誌 檔案
		//MaxBackups: 3,
		// 保存 多少天內的 日誌
		//MaxAge:     28,
		// 要 保存的 日誌 等級 debug info warn error dpanic panic fatal
		Level :"info",
		// 是否要 輸出 代碼位置
		//Caller:true,
	},
}
```

> Redir 只在linux下有效


# 透明代理 

請 確定 linux 內核 在 2.4 以上

透明代理 原理在於 linux 提供的 iptables 實現

1. 使用 iptables 將 tcp 重新定向到 Redir 程式
2. Redir 程式 使用 getsockopt 可以獲取到 原本的訪問目標
3. Redir 爲 原目標 和 訪問者 創建 訪問通道


執行 cat /proc/sys/net/ipv4/ip_forward 查看 是否 開啓了 ip 轉發 如果爲**1** 則 已經開啓

如果 未開啓 編輯 /etc/sysctl.conf 檔案 增加如下內容

```
net.ipv4.ip_forward=1
```

之後執行 `sysctl -p` 使用配置生效 


最後 需要 編輯 一些 iptables 規則 重定向  數據流 

壓縮包 中的 **go-socks5.sh** 已經定義 好了 可參考的 規則 你需要 把 腳本頭部的一些 變量 改爲你自己的 配置 之後 運行 `go-socks5.sh redir` 即可

```bash
#!/bin/bash

# 設置 dns 監聽 端口
# 請修改爲你的 client.jsonnet 中的 DNS.Addr 配置端口
# 如果使用 CoreDNS 則爲 CoreDNS 中的配置 端口
DNS_Port=10053
# 設置 redir 監聽端口 
# 請修改爲你的 client.jsonnet 中的 Proxy.Redir 配置端口
Redir_Port=10900
# 設置 redir 訪問的 外部地址 以便放行
# 請修改爲你的 client.jsonnet 中的 GRPC.Addr 配置服務器地址
Redir_Dst=XXX.XXX.XXX.XXX
```

# CoreDNS

CoreDNS 是 golang 實現的一個 開源 靈活的 插件式 DNS 服務器 建議 使用 CoreDNS 作爲 go-socks5 的前端

因爲 go-socks5 沒有提供 dns 緩存 只是 將 dns 通過代理進行請求

[CoreDNS 下載](https://github.com/coredns/coredns/releases)

CoreDNS 默認 使用當前工作目錄下的 Corefile 作爲配置 檔案

下面是一個 推薦配置 將 dns 轉發到 本地的 127.0.0.1:10054(假設 go-socks5 dns 工作在此地址) 並開啓緩存

```
:10053 {
	cache
	forward . 127.0.0.1:10054
}
```

> 請詳細的 使用方法 請參考 [CoreDNS 官網](https://coredns.io/)

# pptpd vpn

透明代理 只能在linux下使用 且需要修改 系統的防火牆配置 更加推薦的做法是 使用 pptpd 創建一個 vpn 這樣 linux/windows/mac/android/ios 都可以使用 此vpn 訪問 網路

1. 安裝 virtualbox
2. 在 virtualbox 中 安裝一個 內核2.4 以上的 linux 下文都以 ubuntu 爲例子
3. 安裝 pptpd `sudo apt-get install pptpd -y`

    1. 編輯 /etc/pptpd.conf 檔案 配置 localip remoteip 取消掉默認的註釋 即可

		```
		#
		# (Recommended)
		# 服務器 id 
		localip 192.168.0.1
		# 客戶端ip
		remoteip 192.168.0.234-238,192.168.0.245
		# or
		#localip 192.168.0.234-238,192.168.0.245
		#remoteip 192.168.1.234-238,192.168.1.245
		```

	1. 編輯 /etc/ppp/chap-secrets 檔案 設置vpn 連接 用戶名 密碼

		```
		# Secrets for authentication using CHAP
		# client        server  secret                  IP addresses
		# 用戶名        pptpd   密碼            ip (* 自動分配)
		king            pptpd   XXXXXXX     *
		```

	1. 編輯 etc/ppp/pptpd-options 設置dns 地址

		```
		# 請將 dns 配置爲 /etc/pptpd.conf 中的 localip 以便將 pptpd 服務器作爲 dns 服務器
		ms-dns 192.168.0.1
		```

	1. 運行 pptpd 並 加入開機啓動

		```
		sudo systemctl start pptpd.service
		sudo systemctl enable pptpd.service
		```

1. 設置 iptables 將 pptpd 轉發到 redir

	```
	iptables -t nat -A POSTROUTING -s 192.168/16 -o enp0s3 -j MASQUERADE
	```

	> enp0s3 是你用來上網的 網卡設備名 請 改成自己

同樣 壓縮包 中的 **go-socks5.sh** 已經定義 好了 可參考的 規則 你需要 把 腳本頭部的一些 變量 改爲你自己的 配置 之後 運行 `go-socks5.sh vpn` 即可

```bash
#!/bin/bash

# 設置 dns 監聽 端口
# 請修改爲你的 client.jsonnet 中的 DNS.Addr 配置端口
# 如果使用 CoreDNS 則爲 CoreDNS 中的配置 端口
DNS_Port=10053
# 設置 redir 監聽端口 
# 請修改爲你的 client.jsonnet 中的 Proxy.Redir 配置端口
Redir_Port=10900
# 設置 redir 訪問的 外部地址 以便放行
# 請修改爲你的 client.jsonnet 中的 GRPC.Addr 配置服務器地址
Redir_Dst=XXX.XXX.XXX.XXX

# 請修改爲 自己的 網卡設備名 使用 ifconfig 指令可以查看所有 網卡設備
Newwork=enp0s3
```