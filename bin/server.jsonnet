{
	// 服務器 配置 可配置多個 服務器
	Server:[
		{
			// GRPC 配置
			GRPC:{
				// 服務器監聽地址
				Addr:":10700",

				// 客戶端 驗證 密碼
				Password:"cerberus is an idea",
				
				// 定義向 客戶端 發送 ping 以檢測網路狀態 (單位:秒)
				Ping:60,

				// 未活躍連接 被斷開 時間 (單位:秒)
				Timeout:20,
			},
			// 如果爲 true 開啓 dns 轉發
			DNS:true,
			// 路網 配置
			NET:{
				// 請求超時 時間 (單位:秒)
				// 默認 10 秒
				Request:10,
				// 網橋 未活躍 超時 斷線時間  (單位:秒)
				// 默認 5 分鐘
				Bridge:60*5,
				// 每個連接 網路數據包 緩衝區大小
				// 默認 1024 * 32
				Buffer:1024*32
			},
			// 數據傳輸方式 定義
			Transport:{
				// tcp kcp websocket
				Protocol:"tcp",

				// x509 證書路徑
				CertFile:"server.pem",
				KeyFile:"server.key",
			},
		},
		{
			// GRPC 配置
			GRPC:{
				// 服務器監聽地址
				Addr:":18080",

				// 客戶端 驗證 密碼
				Password:"cerberus is an idea",

				// 定義向 客戶端 發送 ping 以檢測網路狀態 (單位:秒)
				Ping:60,

				// 未活躍連接 被斷開 時間 (單位:秒)
				Timeout:20,
			},
			// 如果爲 true 開啓 dns 轉發
			DNS:true,
			// 路網 配置
			NET:{
				// 請求超時 時間 (單位:秒)
				// 默認 10 秒
				Request:10,
				// 網橋 未活躍 超時 斷線時間  (單位:秒)
				// 默認 5 分鐘
				Bridge:60*5,
				// 每個連接 網路數據包 緩衝區大小
				// 默認 1024 * 32
				Buffer:1024*32
			},
			// 數據傳輸方式 定義
			Transport:{
				// tcp kcp websocket
				Protocol:"websocket",

				// x509 證書路徑
				CertFile:"server.pem",
				KeyFile:"server.key",
			},
		},
		{
			// GRPC 配置
			GRPC:{
				// 服務器監聽地址
				Addr:":10700",

				// 客戶端 驗證 密碼
				Password:"cerberus is an idea",

				// 定義向 客戶端 發送 ping 以檢測網路狀態 (單位:秒)
				Ping:60,

				// 未活躍連接 被斷開 時間 (單位:秒)
				Timeout:20,
			},
			// 如果爲 true 開啓 dns 轉發
			DNS:true,
			// 路網 配置
			NET:{
				// 請求超時 時間 (單位:秒)
				// 默認 10 秒
				Request:10,
				// 網橋 未活躍 超時 斷線時間  (單位:秒)
				// 默認 5 分鐘
				Bridge:60*5,
				// 每個連接 網路數據包 緩衝區大小
				// 默認 1024 * 32
				Buffer:1024*32
			},
			// 數據傳輸方式 定義
			Transport:{
				// tcp kcp websocket
				Protocol:"kcp",

				// x509 證書路徑
				CertFile:"server.pem",
				KeyFile:"server.key",
				// kcp 參數
				DataShards:5,
				ParityShards:5,
			},
		},
	],
	Logger:{
		// 日誌 http 如果爲空 則不啓動 http
		//HTTP:"localhost:20700",
		// 日誌 檔案名 如果爲空 則輸出到控制檯
		//Filename:"logs/server.log",
		// 單個日誌檔案 大小上限 MB
		//MaxSize:    100, 
		// 保存 多少個 日誌 檔案
		//MaxBackups: 3,
		// 保存 多少天內的 日誌
		//MaxAge:     28,
		// 要 保存的 日誌 等級 debug info warn error dpanic panic fatal
		Level :"debug",
		// 是否要 輸出 代碼位置
		Caller:true,
	},
}