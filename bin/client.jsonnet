{
	// 代理 配置
	Proxy:{
		// socks5 代理 監聽地址
		// 如果 爲空 則不創建 socks5 代理
		Socks5:"localhost:1080",

		// 透明代理 監聽地址
		// 如果爲空 不 運行透明代理
		// windows 不支持此功能
		Redir:":10900",
	},
	// dns 服務器 配置
	DNS:{
		// dns 服務器 監聽地址
		Addr:":10054",
		// 向哪個 dns 服務器 請求 解析域名
		Server:"8.8.8.8:53",
	},
	// 服務器 配置
	GRPC:{
		// 服務器 地址
		Addr:"127.0.0.1:10700",
		// 密碼
		Password:"cerberus is an idea",

		// 定義向 服務器 發送 ping 以免網路未活躍連接 被斷開 (單位:秒)
		Ping:10,

		// 未活躍連接 被斷開 時間 (單位:秒)
		Timeout:20,
	},
	// 數據傳輸方式 定義
	Transport:{
		// tcp kcp websocket
		Protocol:"tcp",

		// 是否使用 TLS 安全 傳輸
		TLS:true,
		// 是否 忽略 證書驗證
		SkipVerify:true,

		// kcp 參數
		DataShards:5,
		ParityShards:5,
	},
	// 路網 配置
	NET:{
		// 請求超時 時間 (單位:秒)
		// 默認 10 秒
		Request:10,
		// 網橋 未活躍 超時 斷線時間  (單位:秒)
		// 默認 5 分鐘
		Bridge:60*5,
		// 每個連接 網路數據包 緩衝區大小
		// 默認 1024 * 32
		Buffer:1024*32
	},
	Logger:{
		// 日誌 http 如果爲空 則不啓動 http
		//HTTP:"localhost:20800",
		// 日誌 檔案名 如果爲空 則輸出到控制檯
		//Filename:"logs/client.log",
		// 單個日誌檔案 大小上限 MB
		//MaxSize:    100, 
		// 保存 多少個 日誌 檔案
		//MaxBackups: 3,
		// 保存 多少天內的 日誌
		//MaxAge:     28,
		// 要 保存的 日誌 等級 debug info warn error dpanic panic fatal
		Level :"debug",
		// 是否要 輸出 代碼位置
		Caller:true,
	},
}