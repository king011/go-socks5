#!/bin/bash

# 設置 dns 監聽 端口
# 請修改爲你的 client.jsonnet 中的 DNS.Addr 配置端口
# 如果使用 CoreDNS 則爲 CoreDNS 中的配置 端口
DNS_Port=10053
# 設置 redir 監聽端口 
# 請修改爲你的 client.jsonnet 中的 Proxy.Redir 配置端口
Redir_Port=10900
# 設置 redir 訪問的 外部地址 以便放行
# 請修改爲你的 client.jsonnet 中的 GRPC.Addr 配置服務器地址
Redir_Dst=XXX.XXX.XXX.XXX

# 請修改爲 自己的 網卡設備名 使用 ifconfig 指令可以查看所有 網卡設備
Newwork=enp0s3

function ClearRules(){
	# 清空 iptables 規則
	iptables -t nat -F
	iptables -t filter -F
	iptables -t mangle -F
}

function InitRules(){
	# 清空 iptables 規則
	iptables -t nat -F
	iptables -t filter -F
	iptables -t mangle -F

	# 創建 nat/tcp 轉發鏈 用於 轉發 tcp流
	iptables-save | egrep "^\:NAT-TCP" >> /dev/null
	if [[ $? != 0 ]];then
		iptables -t nat -N NAT-TCP
	fi

	# 放行 到 redir 的tcp連接
	iptables -A INPUT -p tcp -m state --state NEW -m tcp --dport $Redir_Port -j ACCEPT

	# 將 dns 查詢 $DNS_Port
	#iptables -t nat -A PREROUTING -p udp --dport 53 -j REDIRECT --to-port 127.0.0.1:$DNS_Port
	#iptables -t nat -A PREROUTING -p tcp --dport 53 -j REDIRECT --to-port 127.0.0.1:$DNS_Port
	iptables -t nat -A OUTPUT -p udp -m udp --dport 53 -j DNAT --to-destination 127.0.0.1:$DNS_Port
	iptables -t nat -A OUTPUT -p tcp -m tcp --dport 53 -j DNAT --to-destination 127.0.0.1:$DNS_Port
	
	# 放行 環回地址 保留地址 特殊地址
	iptables -t nat -A NAT-TCP -d 0/8 -j RETURN
	iptables -t nat -A NAT-TCP -d 127/8 -j RETURN
	iptables -t nat -A NAT-TCP -d 10/8 -j RETURN
	iptables -t nat -A NAT-TCP -d 169.254/16 -j RETURN
	iptables -t nat -A NAT-TCP -d 172.16/12 -j RETURN
	iptables -t nat -A NAT-TCP -d 192.168/16 -j RETURN
	iptables -t nat -A NAT-TCP -d 224/4 -j RETURN
	iptables -t nat -A NAT-TCP -d 240/4 -j RETURN

	# 放行 發往 服務器的 數據
	iptables -t nat -A NAT-TCP -d $Redir_Dst -j RETURN

	# 重定向 tcp 數據包到 redir 監聽端口
	iptables -t nat -A NAT-TCP -p tcp -j REDIRECT --to-ports $Redir_Port

	# 本機 tcp 數據包流向 NAT-TCP 鏈
	iptables -t nat -A OUTPUT -p tcp -j NAT-TCP

	# 內網 tcp 數據包流向 NAT-TCP 鏈
	iptables -t nat -A PREROUTING -p tcp -s 192.168/16 -j NAT-TCP


	# 內網數據包源 NAT
	iptables -t nat -A POSTROUTING -s 192.168/16 -j MASQUERADE
	# 如果 不使用 pptpd 創建 vpn 可 註釋掉這句
	if [[ $1 == "vpn" ]];then
		iptables -t nat -A POSTROUTING -s 192.168/16 -o $Newwork -j MASQUERADE
	fi	
}
function InitDNS(){
	iptables -t nat -F
	iptables -t filter -F
	iptables -t mangle -F

	iptables -t nat -A OUTPUT -p udp -m udp --dport 53 -j DNAT --to-destination 127.0.0.1:$DNS_Port
	iptables -t nat -A OUTPUT -p tcp -m tcp --dport 53 -j DNAT --to-destination 127.0.0.1:$DNS_Port
}
# 顯示幫助信息
function DisplyHelp(){
	echo "help        : display help"
    echo "c/clear     : clear iptable rules"
    echo "r/redir     : set iptable rules for redir"
    echo "v/vpn       : set iptable rules for vpn"
	echo "d/dns       : only set dns iptable rules"
	echo "u/user      : create user for go-socks5"
}

case $1 in
	c|clear)
		ClearRules
	;;

	r|redir)
		InitRules
	;;

	v|vpn)
		InitRules vpn
	;;

	d|dns)
		InitDNS
	;;

	u|user)
		useradd go-socks5 -rs /usr/sbin/nologin
	;;

	*)
		DisplyHelp
		exit 0
	;;
esac