#!/bin/bash
#Program:
#       go socks5 for bash completion
#History:
#       2018-12-28 king011 first release
#Email:
#       zuiwuchang@gmail.com
function __king011_go_socks5_client(){
    # 參數定義
    local opts='-d --debug -p --ping -t --transport --remote --password -c --configure -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -c|--configure)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;

        -p|--ping|--remote|--password)
           COMPREPLY=()
        ;;

        -t|--transport)
            local opts_items='tcp tcp-h2 tcp-h2-skip kcp kcp-h2 kcp-h2-skip ws wss wss-skip'
            COMPREPLY=( $(compgen -W "${opts_items}" -- ${cur}) )
        ;;

        # default
        *)
            COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        ;;
    esac
}
function __king011_go_socks5_server(){
    # 參數定義
    local opts='-d --debug -c --configure -h --help'
    case ${COMP_WORDS[COMP_CWORD-1]} in
        -c|--configure)
           _filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
        ;;

        # default
        *)
            COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
        ;;
    esac
}
function __king011_go_socks5(){
    # 獲取 正在輸入的 參數
    local cur=${COMP_WORDS[COMP_CWORD]}
    
    #  輸入 第1個 參數
    if [ 1 == $COMP_CWORD ];then
        local opts="client server --help -h -v --version"
        COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    else
        # switch 子命令
        case ${COMP_WORDS[1]} in
            client)
                __king011_go_socks5_client
            ;;

            server)
               __king011_go_socks5_server
            ;;
        esac
    fi
}
complete -F __king011_go_socks5 go-socks5
complete -F __king011_go_socks5 ./go-socks5
complete -F __king011_go_socks5 go-socks5.exe
complete -F __king011_go_socks5 ./go-socks5.exe
