package cmd

import (
	"context"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/spf13/cobra"
	server "gitlab.com/king011/go-socks5/cmd/client"
	grpc_client "gitlab.com/king011/go-socks5/cmd/client/client"
	"gitlab.com/king011/go-socks5/configure"
	grpc_socks5 "gitlab.com/king011/go-socks5/protocol/socks5"
	"gitlab.com/king011/go-socks5/utils"
)

func init() {
	bashPath := utils.BasePath()
	filename := utils.Abs(bashPath, "client.jsonnet")
	var debug bool
	var remote, password, transport string
	ping := -1
	cmd := &cobra.Command{
		Use:   "client",
		Short: "run client",
		Run: func(cmd *cobra.Command, args []string) {
			cnf, e := loadClientConfigure(filename)
			if e != nil {
				log.Fatalln(e)
			}
			remote = strings.TrimSpace(remote)
			if remote != "" {
				cnf.GRPC.Addr = remote
			}
			if password != "" {
				cnf.GRPC.Password = password
			}

			transport = strings.ToLower(strings.TrimSpace(transport))
			switch transport {
			case "tcp":
				cnf.Transport.Protocol = "tcp"
				cnf.Transport.TLS = false
				cnf.Transport.SkipVerify = false
			case "tcp-h2":
				cnf.Transport.Protocol = "tcp"
				cnf.Transport.TLS = true
				cnf.Transport.SkipVerify = false
			case "tcp-h2-skip":
				cnf.Transport.Protocol = "tcp"
				cnf.Transport.TLS = true
				cnf.Transport.SkipVerify = true

			case "kcp":
				cnf.Transport.Protocol = "kcp"
				cnf.Transport.TLS = false
				cnf.Transport.SkipVerify = false
			case "kcp-h2":
				cnf.Transport.Protocol = "kcp"
				cnf.Transport.TLS = true
				cnf.Transport.SkipVerify = false
			case "kcp-h2-skip":
				cnf.Transport.Protocol = "kcp"
				cnf.Transport.TLS = true
				cnf.Transport.SkipVerify = true

			case "ws":
				cnf.Transport.Protocol = "websocket"
				cnf.Transport.TLS = false
				cnf.Transport.SkipVerify = false
			case "wss":
				cnf.Transport.Protocol = "websocket"
				cnf.Transport.TLS = true
				cnf.Transport.SkipVerify = false
			case "wss-skip":
				cnf.Transport.Protocol = "websocket"
				cnf.Transport.TLS = true
				cnf.Transport.SkipVerify = true
			}
			if ping > -1 {
				var b []byte
				if ping > 0 {
					b = make([]byte, ping)
				}
				clientPing(cnf, b)
			} else {
				if e := server.Run(cnf, debug); e != nil {
					log.Fatalln(e)
				}
			}
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"configure", "c",
		filename, "configure file path",
	)
	flags.BoolVarP(&debug,
		"debug", "d",
		false, "run as debug mode",
	)
	flags.StringVar(&remote,
		"remote",
		"", "remote server address",
	)
	flags.StringVar(&password,
		"password",
		"", "connect password",
	)
	flags.StringVarP(&transport,
		"transport", "t",
		"",
		"transport [tcp tcp-h2 tcp-h2-skip kcp kcp-h2 kcp-h2-skip ws wss wss-skip]",
	)
	flags.IntVarP(&ping,
		"ping", "p",
		-1,
		"ping server",
	)
	rootCmd.AddCommand(cmd)
}
func initClient(cnf *configure.Client) (e error) {
	// 初始化 grpc 客戶端
	var name string
	name, e = grpc_client.InitClient(cnf, nil)
	if e != nil {
		return
	}
	fmt.Println(name, "dial")

	return
}
func clientPing(cnf *configure.Client, b []byte) {
	e := initClient(cnf)
	if e != nil {
		log.Fatalln(e)
	}
	last := time.Now()
	c := grpc_client.NewSocks5Client()
	_, e = c.Ping(context.Background(), &grpc_socks5.PingRequest{
		Data: b,
	})
	if e != nil {
		log.Println(e)
	}
	fmt.Println("ping", len(b), time.Now().Sub(last))
	if e != nil {
		os.Exit(1)
	}

	last = time.Now()
	_, e = c.Ping(context.Background(), &grpc_socks5.PingRequest{
		Data: b,
	})
	if e != nil {
		log.Println(e)
	}
	fmt.Println("ping", len(b), time.Now().Sub(last))
	if e != nil {
		os.Exit(1)
	}
}
func loadClientConfigure(filename string) (pcnf *configure.Client, e error) {
	// 加載 配置
	var cnf configure.Client
	e = cnf.Load(filename)
	if e != nil {
		return
	}
	e = cnf.Format()
	if e != nil {
		return
	}
	pcnf = &cnf
	return
}
