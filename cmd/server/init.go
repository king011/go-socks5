package server

import (
	"time"

	"gitlab.com/king011/go-socks5/configure"
	"gitlab.com/king011/go-socks5/logger"
	"gitlab.com/king011/go-socks5/utils"
	king_io "gitlab.com/king011/king-go/io"
)

// Run .
func Run(filename string, debug bool) (e error) {
	// 加載 配置
	var cnf configure.Server
	e = cnf.Load(filename)
	if e != nil {
		return
	}
	e = cnf.Format(utils.BasePath())
	if e != nil {
		return
	}

	// 初始化 日誌
	logger.Init(utils.BasePath(), &cnf.Logger)

	// 創建 服務
	s := Service{
		started: time.Now(),
		debug:   debug,
	}

	// 創建 grpc 服務
	n := len(cnf.Server)
	if n > 0 {
		runner := make([]_Runer, n)
		for i := 0; i < n; i++ {
			runner[i], e = newServer(&s, debug, &cnf.Server[i])
			if e != nil {
				return
			}
		}
		s.runner = runner
	}

	// 創建 調試 服務
	if debug {
		s.runner = append(s.runner, &_Debug{
			king_io.NewInputReader(),
			&s,
		})
	}

	// 運行 服務
	s.Run()
	return
}
