package server

import (
	"crypto/sha512"
	"fmt"
	"log"
	"net"
	"time"

	"gitlab.com/king011/go-socks5/cmd/server/server"
	cnf_server "gitlab.com/king011/go-socks5/configure/server"
	"gitlab.com/king011/go-socks5/logger"
	grpc_socks5 "gitlab.com/king011/go-socks5/protocol/socks5"
	"gitlab.com/king011/go-socks5/transport"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
	"google.golang.org/grpc/reflection"
)

type _Server struct {
	l     net.Listener
	s     *grpc.Server
	socks *server.Socks5
}

func newServer(srv server.Service, debug bool, cnf *cnf_server.Server) (gs *_Server, e error) {
	// 創建 tcp Listener
	var l net.Listener
	var name string
	switch cnf.Transport.Protocol {
	case "tcp":
		name, l, e = transport.TCPListen(cnf.GRPC.Addr,
			cnf.Transport.CertFile, cnf.Transport.KeyFile,
			cnf.GRPC.Timeout,
		)
	case "kcp":
		name, l, e = transport.KCPListen(cnf.GRPC.Addr,
			cnf.Transport.CertFile, cnf.Transport.KeyFile,
			cnf.GRPC.Timeout,
			cnf.Transport.DataShards, cnf.Transport.ParityShards,
		)
	case "websocket":
		name, l, e = transport.WebsocketListen(cnf.GRPC.Addr,
			cnf.Transport.CertFile, cnf.Transport.KeyFile,
			cnf.GRPC.Timeout,
		)
	default:
		e = fmt.Errorf("transport not support %v", cnf.Transport.Protocol)
		return
	}

	// 創建 grpc 服務器
	var s *grpc.Server
	pwd := sha512.Sum512([]byte(cnf.GRPC.Password))
	s = grpc.NewServer(
		grpc.Creds(
			transport.NewServerCreds(
				nil,
				pwd[:],
			),
		),
		grpc.KeepaliveEnforcementPolicy(keepalive.EnforcementPolicy{
			MinTime:             time.Second,
			PermitWithoutStream: true,
		}),
		grpc.KeepaliveParams(keepalive.ServerParameters{
			Time:    cnf.GRPC.Ping,
			Timeout: cnf.GRPC.Timeout,
		}),
	)
	if ce := logger.Logger.Check(zap.InfoLevel, name+" work"); ce != nil {
		ce.Write(
			zap.String("addr", cnf.GRPC.Addr),
		)
	}
	if !logger.Logger.OutConsole() {
		log.Println(name, "work at", cnf.GRPC.Addr)
	}

	// 註冊 grpc 服務
	socks := server.NewSocks5(srv,
		&grpc_socks5.ServerInfo{
			Name:  name,
			Addr:  cnf.GRPC.Addr,
			Debug: debug,
			DNS:   cnf.DNS,
		},
		cnf.NET.Request, cnf.NET.Bridge,
		cnf.NET.Buffer,
	)
	grpc_socks5.RegisterSocks5Server(s, socks)

	// 註冊路由
	reflection.Register(s)

	// 返回 服務器
	gs = &_Server{
		l:     l,
		s:     s,
		socks: socks,
	}
	return
}

// 運行 grpc
func (s *_Server) Run() {
	s.s.Serve(s.l)
}

// Server .
func (s *_Server) Server() bool {
	return true
}

// Info .
func (s *_Server) Info() *grpc_socks5.ServerInfo {
	return s.socks.GetInfo()
}
