package server

import (
	"fmt"
	"runtime"
	"time"

	grpc_socks5 "gitlab.com/king011/go-socks5/protocol/socks5"
	"gitlab.com/king011/go-socks5/version"
	king_io "gitlab.com/king011/king-go/io"
)

type _Debug struct {
	*king_io.InputReader
	s *Service
}

func (d *_Debug) Server() bool {
	return false
}
func (d *_Debug) Info() *grpc_socks5.ServerInfo {
	return nil
}
func (d *_Debug) Run() {
	var cmd string
	d.displayHelp()
	for {
		cmd = d.ReadString("cmd")
		if cmd == "h" || cmd == "help" {
			d.displayHelp()
		} else if cmd == "l" || cmd == "local" {
			d.displayLocal()
		} else {
			fmt.Fprint(d.Err,
				`not support command
Use "help" for more information about this program
`)
		}
	}
}

func (d *_Debug) displayHelp() {
	fmt.Fprint(d.Out,
		`h/help     display help
l/local    display runtime info
`,
	)
}

func (d *_Debug) displayLocal() {
	s := d.s
	fmt.Fprintf(d.Out,
		`******       Local       ******
Runtime        : %v %v %v
Version        : %v %v
Build          : %v

Running        : %v
CPU            : %v
Goroutine      : %v
CgoCall        : %v
`,
		runtime.GOOS, runtime.GOARCH, runtime.Version(),
		version.Tag, version.Commit,
		version.Date,

		time.Now().Sub(s.started),
		runtime.NumCPU(),
		runtime.NumGoroutine(),
		runtime.NumCgoCall(),
	)

	for i := 0; i < len(s.runner); i++ {
		r := s.runner[i]
		if r.Server() {
			d.displayServer(r)
		}
	}
}
func (d *_Debug) displayServer(r _Runer) {
	info := r.Info()
	if info == nil || !info.Debug {
		return
	}
	fmt.Fprintf(d.Out,
		`
Work           : %v %v
Debug          : %v
DNS            : %v
NumDNS         : %v
NumProxy       : %v
NumRequest     : %v
`,
		info.Name, info.Addr,
		info.Debug,
		info.DNS,

		info.NumDNS,
		info.NumProxy,
		info.NumRequest,
	)
}
