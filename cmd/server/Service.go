package server

import (
	"runtime"
	"sync"
	"time"

	grpc_socks5 "gitlab.com/king011/go-socks5/protocol/socks5"
	"gitlab.com/king011/go-socks5/version"
)

type _Runer interface {
	Run()
	Server() bool
	Info() *grpc_socks5.ServerInfo
}

// Service 主服務
type Service struct {
	// 服務 啓動 時間
	started time.Time

	// 是否處於 調試 模式
	debug bool

	// 要 運行的 服務
	runner []_Runer
}

// Run 運行 主服務
func (s *Service) Run() {
	n := len(s.runner)
	if n == 0 {
		return
	}
	var wait sync.WaitGroup
	wait.Add(n)
	for i := 0; i < n; i++ {
		go s.run(s.runner[i], &wait)
	}
	wait.Wait()
}
func (s *Service) run(r _Runer, wait *sync.WaitGroup) {
	r.Run()
	wait.Done()
}

// Runtime .
func (s *Service) Runtime() (info *grpc_socks5.InfoResponse) {
	info = &grpc_socks5.InfoResponse{
		OS:      runtime.GOOS,
		ARCH:    runtime.GOARCH,
		Runtime: runtime.Version(),

		Tag:    version.Tag,
		Commit: version.Commit,
		Date:   version.Date,

		Started:   s.started.Unix(),
		CPU:       int32(runtime.NumCPU()),
		Goroutine: int64(runtime.NumGoroutine()),
		Cgo:       runtime.NumCgoCall(),
	}
	if !s.debug {
		return
	}

	arrs := make([]*grpc_socks5.ServerInfo, 0, len(s.runner))
	for _, runner := range s.runner {
		if !runner.Server() {
			continue
		}
		info := runner.Info()
		if info != nil {
			arrs = append(arrs, info)
		}
	}
	if len(arrs) != 0 {
		info.Server = arrs
	}
	return
}
