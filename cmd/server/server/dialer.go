package server

import (
	"context"
	"net"

	"gitlab.com/king011/go-socks5/logger"
	grpc_socks5 "gitlab.com/king011/go-socks5/protocol/socks5"
	"go.uber.org/zap"
)

type _Dialer struct {
	ctx    context.Context
	cancel context.CancelFunc
	stream grpc_socks5.Socks5_ConnectServer
	c      net.Conn
}

func (d *_Dialer) dial() (e error) {
	chErr := make(chan error)
	go d.asyncDial(chErr)
	select {
	case <-d.ctx.Done():
		// 已經 完成
		e = d.ctx.Err()
		go d.asyncClose(chErr)
	case e = <-chErr:
	}
	d.cancel()
	return
}
func (d *_Dialer) asyncClose(chErr chan error) {
	e := <-chErr
	if e == nil {
		d.c.Close()
	}
}
func (d *_Dialer) asyncDial(chErr chan error) {
	// 獲取 連接目標
	var request grpc_socks5.ConnectRequest
	e := d.stream.RecvMsg(&request)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "async dial get target"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		chErr <- e
		return
	}
	// 撥號
	var c net.Conn
	var dialer net.Dialer
	c, e = dialer.DialContext(d.ctx, "tcp", request.Addr)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "connect"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		chErr <- e
		return
	}
	// 返回 撥號 成功
	e = d.stream.Send(&grpc_socks5.ConnectResponse{})
	if e != nil {
		c.Close()
		if ce := logger.Logger.Check(zap.WarnLevel, "connect"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		chErr <- e
		return
	}
	d.c = c
	close(chErr)
}
