package server

import (
	"net"
	"sync/atomic"

	grpc_socks5 "gitlab.com/king011/go-socks5/protocol/socks5"
)

func bridge(c net.Conn, stream grpc_socks5.Socks5_ConnectServer,
	b []byte, proxy, request *int32) {
	// 轉發 tcp 數據
	go bridgeProxy(c, stream, proxy)
	if request == nil {
		bridgeRequest(c, stream, b)
	} else {
		atomic.AddInt32(request, 1)
		bridgeRequest(c, stream, b)
		atomic.AddInt32(request, -1)
	}
}
func bridgeRequest(c net.Conn, stream grpc_socks5.Socks5_ConnectServer, b []byte) {
	response := &grpc_socks5.ConnectResponse{}
	var n int
	var e error
	for {
		if n, e = c.Read(b); e != nil {
			break
		} else if n == 0 {
			continue
		} else {
			response.Data = b[:n]
			if e = stream.Send(response); e != nil {
				break
			}
		}
	}
	c.Close()
}
func bridgeProxy(c net.Conn, stream grpc_socks5.Socks5_ConnectServer,
	trace *int32,
) {
	if trace != nil {
		atomic.AddInt32(trace, 1)
	}
	var e error
	var request grpc_socks5.ConnectRequest
	for {
		e = stream.RecvMsg(&request)
		if e != nil {
			break
		} else if len(request.Data) != 0 {
			if _, e = c.Write(request.Data); e != nil {
				break
			}
		}
	}
	c.Close()

	if trace != nil {
		atomic.AddInt32(trace, -1)
	}
}
