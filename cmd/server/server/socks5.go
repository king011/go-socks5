package server

import (
	"context"
	"sync"
	"time"

	"sync/atomic"

	"github.com/miekg/dns"
	"gitlab.com/king011/go-socks5/logger"
	grpc_socks5 "gitlab.com/king011/go-socks5/protocol/socks5"
	"gitlab.com/king011/go-socks5/transport"
	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// Service .
type Service interface {
	Runtime() *grpc_socks5.InfoResponse
}

// Socks5 代理服務器
type Socks5 struct {
	info           grpc_socks5.ServerInfo
	requestTimeout time.Duration
	bridgeTimeout  time.Duration
	bufferLen      int
	mutex          sync.Mutex

	s Service
}

// NewSocks5 .
func NewSocks5(s Service,
	info *grpc_socks5.ServerInfo,
	requestTimeout, bridgeTimeout time.Duration,
	bufferLen int,
) *Socks5 {
	return &Socks5{
		info: *info,

		requestTimeout: requestTimeout,
		bridgeTimeout:  bridgeTimeout,
		bufferLen:      bufferLen,

		s: s,
	}
}

// GetInfo .
func (s *Socks5) GetInfo() *grpc_socks5.ServerInfo {
	if !s.info.Debug {
		return nil
	}
	var info grpc_socks5.ServerInfo
	s.mutex.Lock()
	info = s.info
	s.mutex.Unlock()
	return &info
}

// DNS 轉發 dns 請求
func (s *Socks5) DNS(ctx context.Context, request *grpc_socks5.DNSRequest) (response *grpc_socks5.DNSResponse, e error) {
	if !s.info.DNS {
		e = status.Error(codes.PermissionDenied, "server disabled dns request")
		return
	}

	// 最晚 超時 時間
	maxDeadline := time.Now().Add(s.requestTimeout)
	var cancel context.CancelFunc

	deadline, ok := ctx.Deadline()
	if ok {
		if deadline.After(maxDeadline) {
			ctx, cancel = context.WithDeadline(ctx, maxDeadline)
		}
	} else {
		ctx, cancel = context.WithDeadline(ctx, maxDeadline)
	}

	if s.info.Debug {
		atomic.AddInt32(&s.info.NumDNS, 1)
		response, e = s.dns(ctx, request)
		atomic.AddInt32(&s.info.NumDNS, -1)
	} else {
		response, e = s.dns(ctx, request)
	}
	if cancel != nil {
		cancel()
	}
	return
}
func (s *Socks5) dns(ctx context.Context, request *grpc_socks5.DNSRequest) (response *grpc_socks5.DNSResponse, e error) {
	// 創建 dns 客戶端
	client := &dns.Client{}
	if request.TCP {
		client.Net = "tcp"
	}
	// 發送 請求
	q := &dns.Msg{}
	e = q.Unpack(request.Data)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "dnsUnpack"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	var in *dns.Msg
	in, _, e = client.ExchangeContext(ctx, q, request.DNS)
	if e != nil {
		return
	}
	// 返回 響應
	var b []byte
	b, e = in.Pack()
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "Pack"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	response = &grpc_socks5.DNSResponse{
		Data: b,
	}
	return
}

// Connect 執行 Connect 建立網橋
func (s *Socks5) Connect(stream grpc_socks5.Socks5_ConnectServer) (e error) {
	ctx, cancel := context.WithTimeout(context.Background(), s.requestTimeout)
	d := &_Dialer{
		ctx:    ctx,
		cancel: cancel,
		stream: stream,
	}
	e = d.dial()
	if e != nil {
		return
	}

	// 建立 網橋
	b := make([]byte, s.bufferLen)
	cc := transport.NewConn(d.c, s.bridgeTimeout)
	if s.info.Debug {
		bridge(cc, stream,
			b,
			&s.info.NumProxy, &s.info.NumRequest,
		)
	} else {
		bridge(cc, stream,
			b,
			nil, nil,
		)
	}
	return
}

// Info 返回 服務器 運行 信息
func (s *Socks5) Info(ctx context.Context, request *grpc_socks5.InfoRequest) (response *grpc_socks5.InfoResponse, e error) {
	response = s.s.Runtime()
	if response == nil {
		e = status.Error(codes.PermissionDenied, "server disabled info request")
	}
	return
}

// Ping 向服務器 發送一個 數據包 並等待服務器 返回原包 用來測試 延遲
func (s *Socks5) Ping(ctx context.Context, request *grpc_socks5.PingRequest) (response *grpc_socks5.PingResponse, e error) {
	response = &grpc_socks5.PingResponse{
		Data: request.Data,
	}
	return
}
