package cmd

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/king011/go-socks5/cmd/server"
	"gitlab.com/king011/go-socks5/utils"
)

func init() {
	bashPath := utils.BasePath()
	filename := utils.Abs(bashPath, "server.jsonnet")
	var debug bool
	cmd := &cobra.Command{
		Use:   "server",
		Short: "run server",
		Run: func(cmd *cobra.Command, args []string) {
			if e := server.Run(filename, debug); e != nil {
				log.Fatalln(e)
			}
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"configure", "c",
		filename, "configure file path",
	)
	flags.BoolVarP(&debug,
		"debug", "d",
		false, "run as debug mode",
	)
	rootCmd.AddCommand(cmd)
}
