package client

import (
	"context"
	"fmt"
	"runtime"
	"strconv"
	"time"

	grpc_client "gitlab.com/king011/go-socks5/cmd/client/client"
	grpc_socks5 "gitlab.com/king011/go-socks5/protocol/socks5"
	king_io "gitlab.com/king011/king-go/io"
	"gitlab.com/king011/share/version"
)

// Trace 跟蹤服務 運行 情況
type Trace struct {
	DNSTCP int32
	DNSUDP int32

	RedirProxy   int32
	RedirRequest int32

	Socks5Proxy    int32
	Socks5Request  int32
	Socks4aProxy   int32
	Socks4aRequest int32
}
type _Debug struct {
	*king_io.InputReader
	s       *Service
	timeout time.Duration
}

func (d *_Debug) Run() {
	grpc_client.WaitFirstDial()
	var cmd string
	d.displayHelp()
	for {
		cmd = d.ReadString("cmd")
		if cmd == "h" || cmd == "help" {
			d.displayHelp()
		} else if cmd == "l" || cmd == "local" {
			d.displayLocal()
		} else if cmd == "r" || cmd == "remote" {
			d.displayRemote()
		} else if cmd == "p" || cmd == "ping" {
			d.debugPing()
		} else {
			fmt.Fprint(d.Err,
				`not support command
Use "help" for more information about this program
`)
		}
	}
}
func (d *_Debug) displayHelp() {
	fmt.Fprint(d.Out,
		`h/help     display help
l/local    display runtime info
r/remote   display remote runtime info
p/ping     send number length data for ping
`,
	)
}
func (d *_Debug) displayLocal() {
	s := d.s

	fmt.Fprintf(d.Out,
		`******       Local       ******
Runtime        : %v %v %v
Version        : %v %v
Build          : %v

Running        : %v
CPU            : %v
Goroutine      : %v
CgoCall        : %v

DNSTCP         : %v
DNSUDP         : %v
Socks5Proxy    : %v
Socks5Request  : %v
Socks4aProxy   : %v
Socks4aRequest : %v
RedirProxy     : %v
RedirRequest   : %v
`,
		runtime.GOOS, runtime.GOARCH, runtime.Version(),
		version.Tag, version.Commit,
		version.Date,
		time.Now().Sub(s.started),
		runtime.NumCPU(),
		runtime.NumGoroutine(),
		runtime.NumCgoCall(),

		s.trace.DNSTCP,
		s.trace.DNSUDP,
		s.trace.Socks5Proxy,
		s.trace.Socks5Request,
		s.trace.Socks4aProxy,
		s.trace.Socks4aRequest,
		s.trace.RedirProxy,
		s.trace.RedirRequest,
	)
}
func (d *_Debug) displayRemote() {
	client := grpc_client.NewSocks5Client()
	ctx, cancel := context.WithTimeout(context.Background(), d.timeout)
	response, e := client.Info(ctx, &grpc_socks5.InfoRequest{})
	cancel()
	if e != nil {
		fmt.Fprintln(d.Err, e)
		return
	}
	fmt.Fprintf(d.Out, `******       Remote       ******
Runtime        : %v %v %v
Version        : %v %v
Build          : %v

Running        : %v
CPU            : %v
Goroutine      : %v
CgoCall        : %v
`,
		response.OS, response.ARCH, response.Runtime,
		response.Tag, response.Commit,
		response.Date,

		time.Now().Sub(time.Unix(response.Started, 0)),
		response.CPU,
		response.Goroutine,
		response.Cgo,
	)

	for i := 0; i < len(response.Server); i++ {
		info := response.Server[i]
		if info == nil {
			continue
		}
		fmt.Fprintf(d.Out,
			`
Work           : %v %v
Debug          : %v
DNS            : %v
NumDNS         : %v
NumProxy       : %v
NumRequest     : %v
`,
			info.Name, info.Addr,
			info.Debug,
			info.DNS,

			info.NumDNS,
			info.NumProxy,
			info.NumRequest,
		)
	}
}
func (d *_Debug) debugPing() {
	fmt.Fprintln(d.Out, `Input ping data len
Enter b to return`)
	for {
		str := d.ReadString("ping")
		if str == "b" {
			break
		}
		n, _ := strconv.ParseUint(str, 10, 16)
		last := time.Now()
		e := d.donePing(int(n))
		if e != nil {
			fmt.Fprintln(d.Err, e)
		}
		fmt.Fprintln(d.Out, n, time.Now().Sub(last))
	}
	return
}
func (d *_Debug) donePing(n int) (e error) {
	client := grpc_client.NewSocks5Client()
	ctx, cancel := context.WithTimeout(context.Background(), d.timeout)
	_, e = client.Ping(ctx, &grpc_socks5.PingRequest{
		Data: make([]byte, n),
	})
	cancel()
	return
}
