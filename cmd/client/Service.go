package client

import (
	"sync"
	"time"
)

type _Runer interface {
	Run()
}

// Service 主服務
type Service struct {
	// 服務 啓動 時間
	started time.Time

	// 是否處於 調試 模式
	debug bool

	// 傳輸 協議 名稱
	name string

	// 跟蹤信息
	trace Trace

	// 要 運行的 服務
	runner []_Runer
}

// Run 運行 主服務
func (s *Service) Run() {
	n := len(s.runner)
	if n == 0 {
		return
	}
	var wait sync.WaitGroup
	wait.Add(n)
	for i := 0; i < n; i++ {
		go s.run(s.runner[i], &wait)
	}
	wait.Wait()
}
func (s *Service) run(r _Runer, wait *sync.WaitGroup) {
	r.Run()
	wait.Done()
}
