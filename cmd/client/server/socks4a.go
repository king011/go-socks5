package server

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"net"

	"gitlab.com/king011/go-socks5/logger"
	grpc_socks5 "gitlab.com/king011/go-socks5/protocol/socks5"
	"go.uber.org/zap"
)

func (s *_Socks) init4a(b []byte) (e error) {
	// 讀取 目標
	_, e = io.ReadFull(s.c, b[2:8])
	if e != nil {
		return
	}

	b[0] = 0
	switch b[1] {
	case 0x01:
		e = s.readSocks4(b)
		return
	case 0x02:
		e = errNotSupportBIND
		logger.Logger.Warn("not support BIND")
	default:
		if ce := logger.Logger.Check(zap.WarnLevel, "socks4a not support unknow CMD"); ce != nil {
			ce.Write(
				zap.Uint8("val", b[1]),
			)
		}
		e = fmt.Errorf("socks4a not support CMD %v", b[1])
	}
	b[1] = 91
	// 通知 失敗
	s.response = b[:8]
	return
}
func (s *_Socks) readSocks4(b []byte) (e error) {
	port := binary.BigEndian.Uint16(b[2:])
	if b[4] == 0 && b[5] == 0 && b[6] == 0 && b[7] != 0 {
		e = s.initSocks4a(b, port)
	} else {
		addr := net.IPv4(b[4], b[5], b[6], b[7])
		dst := fmt.Sprintf("%v:%v", addr, port)
		e = s.initSocks4(b, dst)
	}
	return
}
func (s *_Socks) initSocks4(b []byte, dst string) (e error) {
	// 讀取 uid
	buf := b[8:]
	pos := 0
	var n int
	for {
		if pos == len(buf) {
			if ce := logger.Logger.Check(zap.WarnLevel, "initSocks4"); ce != nil {
				ce.Write(
					zap.String("error", "not buffer read uid"),
				)
			}
			return
		}
		n, e = s.c.Read(buf[pos:])
		if e != nil {
			if ce := logger.Logger.Check(zap.WarnLevel, "initSocks4"); ce != nil {
				ce.Write(
					zap.Error(e),
				)
			}
			return
		} else if n == 0 {
			continue
		}
		pos += n
		if buf[pos-1] == 0 {
			break
		}
	}
	s.response = b[:8]
	var stream grpc_socks5.Socks5_ConnectClient
	stream, e = s.getStream(dst)
	if e != nil {
		return
	}
	s.stream = stream
	return
}
func (s *_Socks) initSocks4a(b []byte, port uint16) (e error) {
	// 讀取 uid
	buf := b[8:]
	pos := 0
	var n int
	first := -1
	for {
		if pos == len(buf) {
			if ce := logger.Logger.Check(zap.WarnLevel, "initSocks4"); ce != nil {
				ce.Write(
					zap.String("error", "not buffer read uid"),
				)
			}
			return
		}
		n, e = s.c.Read(buf[pos:])
		if e != nil {
			if ce := logger.Logger.Check(zap.WarnLevel, "initSocks4"); ce != nil {
				ce.Write(
					zap.Error(e),
				)
			}
			return
		} else if n == 0 {
			continue
		}
		pos += n
		flag := pos - 1
		if buf[flag] == 0 {
			if first != -1 {
				break
			}
			first = bytes.IndexByte(buf[:pos], 0)
			if first != flag {
				break
			}
		}
	}
	buf = buf[first:pos]

	dst := fmt.Sprintf("%s:%v", buf, port)
	s.response = b[:8]
	var stream grpc_socks5.Socks5_ConnectClient
	stream, e = s.getStream(dst)
	if e != nil {
		return
	}
	s.stream = stream
	return
}
