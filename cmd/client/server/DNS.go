package server

import (
	"context"
	"log"
	"net"
	"sync"
	"time"

	"sync/atomic"

	"github.com/miekg/dns"
	grpc_client "gitlab.com/king011/go-socks5/cmd/client/client"
	"gitlab.com/king011/go-socks5/logger"
	grpc_socks5 "gitlab.com/king011/go-socks5/protocol/socks5"
	"go.uber.org/zap"
)

// DNS .
type DNS struct {
	l       net.Listener
	p       net.PacketConn
	server  string
	timeout time.Duration

	// 跟蹤 dns 請求
	tcp *int32
	udp *int32
}

// NewDNS 創建 dns 服務器
func NewDNS(laddr, server string, timeout time.Duration, tcp, udp *int32) (d *DNS, e error) {
	// 監聽 udp
	u, e := net.ResolveUDPAddr("udp", laddr)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "NewDNS"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	p, e := net.ListenUDP("udp", u)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "NewDNS"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	if !logger.Logger.OutConsole() {
		log.Println("udp dns work at", laddr)
	}
	if ce := logger.Logger.Check(zap.InfoLevel, "udp dns work"); ce != nil {
		ce.Write(
			zap.String("addr", laddr),
		)
	}

	// 監聽 tcp
	l, e := net.Listen("tcp", laddr)
	if e != nil {
		p.Close()
		if ce := logger.Logger.Check(zap.ErrorLevel, "NewDNS"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	if !logger.Logger.OutConsole() {
		log.Println("tcp dns work at", laddr)
	}
	if ce := logger.Logger.Check(zap.InfoLevel, "tcp dns work"); ce != nil {
		ce.Write(
			zap.String("addr", laddr),
		)
	}

	d = &DNS{
		p:       p,
		l:       l,
		server:  server,
		timeout: timeout,

		tcp: tcp,
		udp: udp,
	}
	return
}

// Run 運行 dns 服務
func (d *DNS) Run() {
	if d.l == nil || d.p == nil {
		return
	}

	var wait sync.WaitGroup
	wait.Add(2)
	go func() {
		dns.ActivateAndServe(d.l, nil, &_DNSHandler{
			TCP:     true,
			Server:  d.server,
			Timeout: d.timeout,

			trace: d.tcp,
		})
		wait.Done()
	}()
	go func() {
		dns.ActivateAndServe(nil, d.p, &_DNSHandler{
			TCP:     false,
			Server:  d.server,
			Timeout: d.timeout,

			trace: d.udp,
		})
		wait.Done()
	}()
	wait.Wait()
}

type _DNSHandler struct {
	TCP     bool
	Server  string
	Timeout time.Duration

	trace *int32
}

// ServeDNS 實現 dns.Handler 接口 響應dns 請求
func (d *_DNSHandler) ServeDNS(w dns.ResponseWriter, r *dns.Msg) {
	if d.trace == nil {
		d.serveDNS(w, r)
	} else {
		atomic.AddInt32(d.trace, 1)
		d.serveDNS(w, r)
		atomic.AddInt32(d.trace, -1)
	}
}
func (d *_DNSHandler) serveDNS(w dns.ResponseWriter, r *dns.Msg) {
	b, e := r.Pack()
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "DNS Pack"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	client := grpc_client.NewSocks5Client()
	var response *grpc_socks5.DNSResponse
	if d.Timeout > 0 {
		ctx, cancel := context.WithTimeout(context.Background(), d.Timeout)
		response, e = client.DNS(ctx, &grpc_socks5.DNSRequest{
			Data: b,
			DNS:  d.Server,
		})
		cancel()
	} else {
		response, e = client.DNS(context.Background(), &grpc_socks5.DNSRequest{
			Data: b,
			DNS:  d.Server,
		})
	}
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "DNS"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	var in dns.Msg
	e = in.Unpack(response.Data)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "DNS Unpack"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	e = w.WriteMsg(&in)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "DNS Write"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
}
