package server

import (
	"context"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"time"

	"runtime"

	grpc_client "gitlab.com/king011/go-socks5/cmd/client/client"
	"gitlab.com/king011/go-socks5/logger"
	grpc_socks5 "gitlab.com/king011/go-socks5/protocol/socks5"
	"gitlab.com/king011/go-socks5/transport"
	"go.uber.org/zap"
)

var errNotFoundSupportMETHODS = errors.New("not found support METHODS")
var errNotSupportBIND = errors.New("not support BIND")
var errNotSupportUDP = errors.New("not support UDP")

// Socks5 代理服務器實現
type Socks5 struct {
	l net.Listener

	// 請求超時 時間 (單位:秒)
	// 默認 10 秒
	requestTimeout time.Duration
	// 網橋 未活躍 超時 斷線時間  (單位:秒)
	// 默認 5 分鐘
	bridgeTimeout time.Duration
	// 每個連接 網路數據包 緩衝區大小
	// 默認 1024 * 32
	bufferSize int

	proxy5    *int32
	request5  *int32
	proxy4a   *int32
	request4a *int32
}

// NewSocks5 創建一個 socks5 服務器
func NewSocks5(laddr string,
	requestTimeout, bridgeTimeout time.Duration,
	bufferSize int,
	proxy5, request5, proxy4a, request4a *int32,
) (s *Socks5, e error) {
	var l net.Listener
	l, e = net.Listen("tcp", laddr)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "NewSocks5"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}

	if !logger.Logger.OutConsole() {
		log.Println("socks5 work at", laddr)
	}
	if ce := logger.Logger.Check(zap.InfoLevel, "socks5 work"); ce != nil {
		ce.Write(
			zap.String("laddr", laddr),
		)
	}

	s = &Socks5{
		l: l,

		requestTimeout: requestTimeout,
		bridgeTimeout:  bridgeTimeout,
		bufferSize:     bufferSize,

		proxy5:    proxy5,
		request5:  request5,
		proxy4a:   proxy4a,
		request4a: request4a,
	}
	return
}

// Run 運行 服務
func (s *Socks5) Run() {
	l := s.l
	for {
		c, e := l.Accept()
		if e != nil {
			if ce := logger.Logger.Check(zap.WarnLevel, "Accept"); ce != nil {
				ce.Write(
					zap.Error(e),
					zap.Int("NumGoroutine", runtime.NumGoroutine()),
				)
			}
			time.Sleep(time.Second)
			continue
		}

		// 建立 連接
		go s.initAccept(c)
	}
}

func (s *Socks5) initAccept(c net.Conn) {
	socks := &_Socks{
		c:       c,
		timeout: s.requestTimeout,
	}
	e := socks.openStream()
	if e != nil {
		if socks.response != nil && e != context.DeadlineExceeded {
			_, e = c.Write(socks.response)
			if e != nil {
				time.Sleep(time.Millisecond * 100)
			}
		}
		c.Close()
		return
	}
	b := make([]byte, s.bufferSize)
	cc := transport.NewConn(c, s.bridgeTimeout)
	if socks.v5 {
		bridge(cc, socks.stream,
			socks.cancel, b,
			s.proxy5, s.request5,
		)
	} else {
		bridge(cc, socks.stream,
			socks.cancel, b,
			s.proxy4a, s.request4a,
		)
	}

}

type _Socks struct {
	c       net.Conn
	timeout time.Duration

	response []byte
	stream   grpc_socks5.Socks5_ConnectClient

	ctx    context.Context
	cancel context.CancelFunc

	v5 bool
}

func (s *_Socks) openStream() (e error) {
	ch := make(chan error, 1)
	timer := time.NewTimer(s.timeout)
	ctx, cancel := context.WithCancel(context.Background())
	s.ctx, s.cancel = ctx, cancel
	go s.asyncInit(ch)
	select {
	case <-ctx.Done():
		// 已經 完成
		if !timer.Stop() {
			<-timer.C
		}
		e = context.Canceled
	case <-timer.C:
		cancel()
		e = context.DeadlineExceeded
		if ce := logger.Logger.Check(zap.WarnLevel, "Socks OpenStream"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
	case e = <-ch:
		if !timer.Stop() {
			<-timer.C
		}
		if e != nil {
			cancel()
		}
	}
	return
}
func (s *_Socks) asyncInit(ch chan error) {
	ch <- s.init()
}
func (s *_Socks) init() (e error) {
	// 只要注意下 實現 緩衝區 最大時 就是 接收 socks5請求 時 的 請求/響應 包
	// VER	CMD	RSV	ATYP	DST.ADDR	DST.PORT
	// 1    1   1   1       1+255       2
	//b := make([]byte, 262)
	b := make([]byte, 512)
	_, e = io.ReadFull(s.c, b[:2])
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "initAccept"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	// 驗證 版本
	if b[0] == 0x4 {
		e = s.init4a(b)
		return
	} else if b[0] != 0x5 {
		e = fmt.Errorf("not support VER %v", b[0])
		if ce := logger.Logger.Check(zap.WarnLevel, "VER must be 0x05 or 0x04"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		s.notSupportNMETHODS(b)
		return
	}
	s.v5 = true
	// 獲取 客戶端 支持的 認證方式
	if b[1] < 1 || b[1] > 255 {
		e = fmt.Errorf("not support NMETHODS %v", b[1])
		if ce := logger.Logger.Check(zap.WarnLevel, "NMETHODS must range [1,255]"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		s.notSupportNMETHODS(b)
		return
	}
	n := int(b[1])
	_, e = io.ReadFull(s.c, b[:n])
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "Read"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	// 查找 支持的 認證
	for _, v := range b[:n] {
		if v == 0x00 {
			// 無需認證
			// 當 沒有配置用戶時 才 允許 無需認證

			// 通知 無需驗證
			b[0] = 0x05
			b[1] = 0x00
			_, e = s.c.Write(b[:2])
			if e != nil {
				if ce := logger.Logger.Check(zap.WarnLevel, "Write"); ce != nil {
					ce.Write(
						zap.Error(e),
					)
				}
				return
			}
			// 等待 客戶端 發送 代理請求
			e = s.waitSocks5Request(b)
			return
		}
	}

	s.notSupportNMETHODS(b)
	e = errNotFoundSupportMETHODS
	return
}

// 協商 認證 失敗
func (s *_Socks) notSupportNMETHODS(b []byte) {
	b[0] = 0x05
	b[1] = 0xFF
	s.response = b
}
func (s *_Socks) waitSocks5Request(b []byte) (e error) {
	// 等待 客戶端 發送 請求
	_, e = io.ReadFull(s.c, b[:4])
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "Read"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	if b[0] != 0x5 {
		if ce := logger.Logger.Check(zap.WarnLevel, "VER must be 0x05"); ce != nil {
			ce.Write(
				zap.Uint8("val", b[0]),
			)
		}
		e = fmt.Errorf("not support VER %v", b[0])
		return
	}

	switch b[1] {
	case 0x01:
		// 執行 CONNECT
		e = s.connect(b)
		return
	case 0x02:
		e = errNotSupportBIND
		logger.Logger.Warn("not support BIND")
	case 0x03:
		e = errNotSupportUDP
		logger.Logger.Warn("not support UDP")
	default:
		if ce := logger.Logger.Check(zap.WarnLevel, "not support unknow CMD"); ce != nil {
			ce.Write(
				zap.Uint8("val", b[1]),
			)
		}
		e = fmt.Errorf("not support CMD %v", b[1])
	}

	// 通知 不支持 指令
	//b[0] = 0x05
	b[1] = 0x07 // 不支持的 命令
	b[2] = 0
	b[3] = 0x01
	s.response = b[:4+4+2]
	return
}

func (s *_Socks) connect(b []byte) (e error) {
	// 返回 請求目標
	var target string
	target, e = s.getTarget(b)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "connect"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	var stream grpc_socks5.Socks5_ConnectClient
	stream, e = s.getStream(target)
	if e != nil {
		return
	}
	s.stream = stream
	return
}
func (s *_Socks) getStream(addr string) (stream grpc_socks5.Socks5_ConnectClient, e error) {
	// 連接 目標
	client := grpc_client.NewSocks5Client()
	stream, e = client.Connect(s.ctx)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "connect"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		if s.v5 {
			s.response[1] = 0x01
		} else {
			s.response[1] = 91
		}
		return
	}
	e = stream.Send(&grpc_socks5.ConnectRequest{
		Addr: addr,
	})
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "connect"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		if s.v5 {
			s.response[1] = 0x01
		} else {
			s.response[1] = 91
		}
		return
	}
	_, e = stream.Recv()
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "connect"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		if s.v5 {
			s.response[1] = 0x01
		} else {
			s.response[1] = 91
		}
		return
	}
	// 通知 成功
	if s.v5 {
		s.response[1] = 0x00
	} else {
		s.response[1] = 90
	}
	_, e = s.c.Write(s.response)
	s.response = nil
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "connect"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	return
}
func (s *_Socks) getTarget(b []byte) (target string, e error) {
	switch b[3] {
	case 0x01: //ipv4
		_, e = io.ReadFull(s.c, b[4:4+4+2])
		if e != nil {
			return
		}
		addr := net.IPv4(b[4], b[5], b[6], b[7]).String()
		port := binary.BigEndian.Uint16(b[4+4:])
		target = fmt.Sprintf("%s:%v", addr, port)
		s.response = b[:4+4+2]
	case 0x03: //domain
		_, e = io.ReadFull(s.c, b[4:4+1])
		if e != nil {
			return
		}
		n := int(b[4])
		_, e = io.ReadFull(s.c, b[4+1:4+1+n+2])
		addr := b[4+1 : 4+1+n]
		port := binary.BigEndian.Uint16(b[4+1+n:])
		target = fmt.Sprintf("%s:%v", addr, port)
		//response = b[:4+1+n+2]
		s.response = b[:4+4+2]
		s.response[3] = 0x01
	case 0x04: //ipv6
		_, e = io.ReadFull(s.c, b[4:4+16+2])
		if e != nil {
			return
		}
		addr := net.IP(b[4 : 4+16])
		port := binary.BigEndian.Uint16(b[4+16:])
		target = fmt.Sprintf("%v:%v", addr, port)
		//response = b[:4+16+2]
		s.response = b[:4+4+2]
		s.response[3] = 0x01
	default:
		e = fmt.Errorf("not support ATYP [0x%02x]", b[3])
	}
	return
}
