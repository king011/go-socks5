package server

import (
	"context"
	"net"
	"sync/atomic"

	grpc_socks5 "gitlab.com/king011/go-socks5/protocol/socks5"
)

func bridge(c net.Conn, stream grpc_socks5.Socks5_ConnectClient,
	cancel context.CancelFunc,
	b []byte, proxy, request *int32) {
	// 轉發 tcp 數據
	go bridgeRequest(c, stream, cancel, request)
	if proxy == nil {
		bridgeProxy(c, stream, cancel, b)
	} else {
		atomic.AddInt32(proxy, 1)
		bridgeProxy(c, stream, cancel, b)
		atomic.AddInt32(proxy, -1)
	}
}
func bridgeProxy(c net.Conn, stream grpc_socks5.Socks5_ConnectClient,
	cancel context.CancelFunc,
	b []byte,
) {
	request := &grpc_socks5.ConnectRequest{}
	var n int
	var e error
	for {
		if n, e = c.Read(b); e != nil {
			break
		} else if n == 0 {
			continue
		} else {
			request.Data = b[:n]
			if e = stream.Send(request); e != nil {
				break
			}
		}
	}
	c.Close()
	cancel()
}
func bridgeRequest(c net.Conn, stream grpc_socks5.Socks5_ConnectClient,
	cancel context.CancelFunc,
	trace *int32,
) {
	if trace != nil {
		atomic.AddInt32(trace, 1)
	}
	var e error
	var response grpc_socks5.ConnectResponse
	for {
		e = stream.RecvMsg(&response)
		if e != nil {
			break
		} else if len(response.Data) != 0 {
			if _, e = c.Write(response.Data); e != nil {
				break
			}
		}
	}
	c.Close()
	cancel()

	if trace != nil {
		atomic.AddInt32(trace, -1)
	}
}
