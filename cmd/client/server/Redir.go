package server

import (
	"context"
	"log"
	"net"
	"syscall"
	"time"

	"runtime"

	grpc_client "gitlab.com/king011/go-socks5/cmd/client/client"
	native_sys "gitlab.com/king011/go-socks5/cmd/client/sys"
	"gitlab.com/king011/go-socks5/logger"
	grpc_socks5 "gitlab.com/king011/go-socks5/protocol/socks5"
	"gitlab.com/king011/go-socks5/transport"
	"go.uber.org/zap"
)

// Redir 透明代理 實現
type Redir struct {
	l net.Listener

	// 請求超時 時間 (單位:秒)
	// 默認 10 秒
	requestTimeout time.Duration
	// 網橋 未活躍 超時 斷線時間  (單位:秒)
	// 默認 5 分鐘
	bridgeTimeout time.Duration
	// 每個連接 網路數據包 緩衝區大小
	// 默認 1024 * 32
	bufferSize int

	proxy   *int32
	request *int32
}

// NewRedir .
func NewRedir(laddr string,
	requestTimeout, bridgeTimeout time.Duration,
	bufferSize int,
	proxy, request *int32,
) (r *Redir, e error) {
	var l net.Listener
	l, e = net.Listen("tcp", laddr)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, "NewRedir"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	if !logger.Logger.OutConsole() {
		log.Println("redir work at", laddr)
	}
	if ce := logger.Logger.Check(zap.InfoLevel, "redir work"); ce != nil {
		ce.Write(
			zap.String("addr", laddr),
		)
	}
	r = &Redir{
		l: l,

		requestTimeout: requestTimeout,
		bridgeTimeout:  bridgeTimeout,

		bufferSize: bufferSize,

		proxy:   proxy,
		request: request,
	}
	return
}

// Run 運行 透明代理
func (r *Redir) Run() {
	l := r.l
	for {
		c, e := l.Accept()
		if e != nil {
			if ce := logger.Logger.Check(zap.WarnLevel, "Redir Accept"); ce != nil {
				ce.Write(
					zap.Error(e),
					zap.Int("NumGoroutine", runtime.NumGoroutine()),
				)
			}
			time.Sleep(time.Second)
			continue
		}

		// 建立 連接
		go r.initAccept(c)
	}
}
func (r *Redir) initAccept(c net.Conn) {
	r.initConnect(c)
	c.Close()
}
func (r *Redir) initConnect(c net.Conn) {
	// 返回 fd
	sys, ok := c.(syscall.Conn)
	if !ok {
		logger.Logger.Warn("Redir net.Conn not implemented syscall.Conn")
		return
	}
	var sysc syscall.RawConn
	sysc, e := sys.SyscallConn()
	if !ok {
		if ce := logger.Logger.Check(zap.WarnLevel, "SyscallConn"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}

	// 返回 原目標
	var addr *net.TCPAddr
	ec := sysc.Control(func(fd uintptr) {
		addr = native_sys.GetDestAddr(int(fd))
	})
	if ec != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "Control fd"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	if addr == nil {
		logger.Logger.Warn("native getsockopt error")
		return
	}
	// 連接 目標
	r.connect(c, addr.String())
}
func (r *Redir) connect(c net.Conn, addr string) {
	ctx, cancel := context.WithCancel(context.Background())
	d := &_Dialer{
		addr:    addr,
		ctx:     ctx,
		cancel:  cancel,
		timeout: r.requestTimeout,
	}
	stream, e := d.Dial()
	if e == nil {
		// 建立 網橋
		b := make([]byte, r.bufferSize)
		cc := transport.NewConn(c, r.bridgeTimeout)

		bridge(cc, stream, cancel, b, r.proxy, r.request)
	} else {
		cancel()
	}
}

type _Dialer struct {
	addr    string
	timeout time.Duration
	ctx     context.Context
	cancel  context.CancelFunc

	stream grpc_socks5.Socks5_ConnectClient
}

func (d *_Dialer) Dial() (stream grpc_socks5.Socks5_ConnectClient, e error) {
	errChannel := make(chan error, 2)
	timer := time.AfterFunc(d.timeout, func() {
		errChannel <- context.DeadlineExceeded
	})
	go d.dial(errChannel)
	e = <-errChannel
	if e != context.DeadlineExceeded {
		timer.Stop()
	}
	if e == nil {
		stream = d.stream
	}
	return
}
func (d *_Dialer) dial(errChannel chan error) {
	var e error
	d.stream, e = d.connectContext(d.ctx, d.addr)
	errChannel <- e
}
func (d *_Dialer) connectContext(ctx context.Context, addr string) (stream grpc_socks5.Socks5_ConnectClient, e error) {
	// 連接 目標
	client := grpc_client.NewSocks5Client()
	stream, e = client.Connect(ctx)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "Redir connect"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}

	e = stream.Send(&grpc_socks5.ConnectRequest{
		Addr: addr,
	})
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "Redir connect"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	_, e = stream.Recv()
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, "Redir connect"); ce != nil {
			ce.Write(
				zap.Error(e),
			)
		}
		return
	}
	return
}
