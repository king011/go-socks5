package client

import (
	"fmt"
	"runtime"
	"time"

	grpc_client "gitlab.com/king011/go-socks5/cmd/client/client"
	"gitlab.com/king011/go-socks5/cmd/client/server"
	"gitlab.com/king011/go-socks5/configure"
	"gitlab.com/king011/go-socks5/logger"
	"gitlab.com/king011/go-socks5/utils"
	king_io "gitlab.com/king011/king-go/io"
)

// Run .
func Run(cnf *configure.Client, debug bool) (e error) {
	// 初始化 日誌
	logger.Init(utils.BasePath(), &cnf.Logger)

	// 初始化 grpc 客戶端
	var name string
	name, e = grpc_client.InitClient(cnf, &logger.Logger)
	if e != nil {
		return
	}
	if !logger.Logger.OutConsole() {
		fmt.Println(name, "dial")
	}
	logger.Logger.Info(name + " dial")

	// 創建 服務
	s := Service{
		started: time.Now(),
		debug:   debug,
		name:    name,
	}

	// 創建 dns 服務
	if cnf.DNS.Addr != "" && cnf.DNS.Server != "" {
		var dns *server.DNS
		if debug {
			dns, e = server.NewDNS(cnf.DNS.Addr,
				cnf.DNS.Server, cnf.NET.Request,
				&s.trace.DNSTCP, &s.trace.DNSUDP,
			)
		} else {
			dns, e = server.NewDNS(cnf.DNS.Addr,
				cnf.DNS.Server, cnf.NET.Request,
				nil, nil,
			)
		}
		if e != nil {
			return
		}
		s.runner = append(s.runner, dns)
	}
	// 創建 socks5 服務
	if cnf.Proxy.Socks5 != "" {
		var socks5 *server.Socks5
		if debug {
			socks5, e = server.NewSocks5(cnf.Proxy.Socks5,
				cnf.NET.Request, cnf.NET.Bridge, cnf.NET.Buffer,
				&s.trace.Socks5Proxy, &s.trace.Socks5Request,
				&s.trace.Socks4aProxy, &s.trace.Socks4aRequest,
			)
		} else {
			socks5, e = server.NewSocks5(cnf.Proxy.Socks5,
				cnf.NET.Request, cnf.NET.Bridge, cnf.NET.Buffer,
				nil, nil,
				nil, nil,
			)
		}
		if e != nil {
			return
		}
		s.runner = append(s.runner, socks5)
	}
	// 創建 透明 代理
	if cnf.Proxy.Redir != "" && runtime.GOOS != "windows" {
		var redir *server.Redir
		if debug {
			redir, e = server.NewRedir(cnf.Proxy.Redir,
				cnf.NET.Request, cnf.NET.Bridge, cnf.NET.Buffer,
				&s.trace.RedirProxy, &s.trace.RedirRequest,
			)
		} else {
			redir, e = server.NewRedir(cnf.Proxy.Redir,
				cnf.NET.Request, cnf.NET.Bridge, cnf.NET.Buffer,
				nil, nil,
			)
		}
		if e != nil {
			return
		}
		s.runner = append(s.runner, redir)
	}

	// 創建 調試 服務
	if debug {
		s.runner = append(s.runner, &_Debug{
			king_io.NewInputReader(),
			&s,
			cnf.NET.Request,
		})
	}
	// 運行服務
	s.Run()

	return
}
