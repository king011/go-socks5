package client

import (
	"crypto/sha512"
	"fmt"
	"net"
	"time"

	"sync"

	"gitlab.com/king011/go-socks5/configure"
	grpc_socks5 "gitlab.com/king011/go-socks5/protocol/socks5"
	"gitlab.com/king011/go-socks5/transport"
	logger "gitlab.com/king011/king-go/log/logger.zap"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
)

var _Client Client
var _df transport.DialF
var _name string
var _logger *logger.Logger
var _ch = make(chan struct{})
var _first = true
var _m sync.Mutex

// Client 客戶端
type Client struct {
	*grpc.ClientConn
}

// WaitFirstDial .
func WaitFirstDial() {
	<-_ch
}

// InitClient 創建 grpc 客戶端
func InitClient(cnf *configure.Client, logger *logger.Logger) (name string, e error) {
	_logger = logger
	switch cnf.Transport.Protocol {
	case "tcp":
		name, _df = transport.TCPDial(cnf.Transport.TLS, cnf.Transport.SkipVerify)
	case "kcp":
		dialer := transport.KCPDial(cnf.Transport.TLS,
			cnf.Transport.SkipVerify,
			cnf.Transport.DataShards, cnf.Transport.ParityShards,
		)
		name, _df = dialer.Name, dialer.Dial
	case "websocket":
		name, _df = transport.WebsocketDial(cnf.Transport.TLS, cnf.Transport.SkipVerify)
	default:
		e = fmt.Errorf("transport not support %v", cnf.Transport.Protocol)
		return
	}
	_name = name
	conn, e := dial(cnf)
	if e != nil {
		return
	}
	_Client = Client{
		conn,
	}
	return
}
func _Dial(addr string, timeout time.Duration) (net.Conn, error) {
	if _logger == nil {
		return _df(addr, timeout)
	}
	if ce := _logger.Check(zap.InfoLevel, "Dial"); ce != nil {
		ce.Write(
			zap.String("name", _name),
			zap.String("addr", addr),
			zap.String("timeout", timeout.String()),
		)
	}
	c, e := _df(addr, timeout)
	if e == nil {
		if ce := _logger.Check(zap.InfoLevel, "Dial Success"); ce != nil {
			ce.Write(
				zap.String("name", _name),
				zap.String("addr", addr),
				zap.String("timeout", timeout.String()),
			)
		}
		_m.Lock()
		if _first {
			close(_ch)
			_first = false
		}
		_m.Unlock()
	} else {
		if ce := _logger.Check(zap.WarnLevel, "Dial Fault"); ce != nil {
			ce.Write(
				zap.String("name", _name),
				zap.String("addr", addr),
				zap.String("timeout", timeout.String()),
				zap.Error(e),
			)
		}
	}
	return c, e
}
func dial(cnf *configure.Client) (conn *grpc.ClientConn, e error) {
	pwd := sha512.Sum512([]byte(cnf.GRPC.Password))
	opts := make([]grpc.DialOption, 0, 10)
	opts = append(opts,
		grpc.WithTransportCredentials(
			transport.NewClientCreds(nil, pwd[:]),
		),
		grpc.WithDialer(_Dial),
		grpc.WithKeepaliveParams(keepalive.ClientParameters{
			Time:                cnf.GRPC.Ping,
			Timeout:             cnf.GRPC.Timeout,
			PermitWithoutStream: true,
		}),
	)
	conn, e = grpc.Dial(
		cnf.GRPC.Addr,
		opts...,
	)
	return
}

// NewSocks5Client .
func NewSocks5Client() grpc_socks5.Socks5Client {
	return grpc_socks5.NewSocks5Client(_Client.ClientConn)
}
